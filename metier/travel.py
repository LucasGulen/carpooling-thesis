from pandas import DataFrame


class Travel:
    
    def __init__(self):
        self.id = ''
        self.driverData: DataFrame = ''
        self.driverWindows: DataFrame = ''
        self.passengerWindows: DataFrame = ''
        self.window_d_p_merged: DataFrame = ''
        self.passengersData: list = []
        self.driverAligned: list = []
        self.passengersAligned: list = []
