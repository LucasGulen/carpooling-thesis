import * as functions from 'firebase-functions';
const cors = require('cors')({origin: true})
const admin = require("firebase-admin");

const serviceAccount = require("../src/serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sensor-collection.firebaseio.com"
});
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const sendNotification = functions.https.onRequest((request, response) => {
    // response.setHeader('Access-Control-Allow-Origin', '*');
    return cors(request, response, () => {
        // *Your code*
        const payload = {
            data: {
                invitationFrom: request.body.username,
                travelToken: request.body.travelToken
            },
            notification: {
                title: 'Invitation à un voyage !',
                body: request.body.username + ' t\'a invité à rejoindre son véhicule !',
                icon: 'default',
                click_action: 'FCM_PLUGIN_ACTIVITY'
            }
        }
        console.log(request.body);
        admin.messaging().sendToDevice(request.body.destinationToken, payload).then( (res: any) => {
            console.log("Successfully sent notification: ", res);
            response.send();
        }).catch((error: any) => {
            console.log("Notification failed: ", error);
            response.send(error);
        });

    });

});
