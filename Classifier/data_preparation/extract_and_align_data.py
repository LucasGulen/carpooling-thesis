import numpy as np
from pandas import DataFrame
import config
from data_preparation.utils import read_data, save_data

# CONSTANTS
data_path = config.constants.get('DATA_PATH')
data_positive_cases = config.constants.get("RAW_POSITIVE_CASES")
data_negative_cases = config.constants.get("RAW_NEGATIVE_CASES")
aligned_dir = config.constants.get('ALIGNED_DIRECTORY')
# CONSTANTS


def get_aligned_data(dataset):
    for travel in dataset:
        for passenger in travel.passengersData:
            # Get the closest driver data aligned to the passenger based on timestamp recording
            driver_aligned, passenger_aligned = align_data(travel.driverData, passenger)
            travel.driverAligned.append(driver_aligned)
            travel.passengersAligned.append(passenger_aligned)
    return dataset


def align_start(driver_timestamps, passenger_timestamp):
    # Search the indexes in driver timestamps where first passenger data is equal to
    driver_similar_index = np.where(driver_timestamps == passenger_timestamp[0])[0]

    # Search the indexes in passenger timestamps where first driver data is equal to
    passenger_similar_index = np.where(passenger_timestamp == driver_timestamps[0])[0]

    if (len(driver_similar_index) != 0):
        return driver_similar_index[0], 0
    elif (len(passenger_similar_index) != 0):
        return 0, passenger_similar_index[0]


def align_end(driver_timestamps, passenger_timestamps):
    nb_frames_driver = driver_timestamps.shape[0]
    nb_frames_passenger = passenger_timestamps.shape[0]

    # If more frames in driver, align it to the max frames of passenger
    if (nb_frames_driver > nb_frames_passenger):
        return nb_frames_passenger - 1, nb_frames_passenger - 1
    elif (nb_frames_passenger > nb_frames_driver):
        # If more frames in passenger, align it to the max frames of driver
        return nb_frames_driver - 1, nb_frames_driver - 1
    else:
        # Else the sizes are equals
        return nb_frames_driver - 1, nb_frames_passenger - 1


def align_data(driver, passenger):
    # Align start indexes
    driver_start, passenger_start = align_start(driver.iloc[:, 10], passenger.iloc[:, 10])
    driver = driver[driver_start:]
    passenger = passenger[passenger_start:]

    # Align end indexes
    driver_end, passenger_end = align_end(driver.iloc[:, 10], passenger.iloc[:, 10])
    driver = driver[:driver_end]
    passenger = passenger[:passenger_end]

    return driver, passenger


def print_dataset_informations(pos_dataset, neg_dataset):
    pos_nb_travel_passengers = 0
    neg_nb_travel_passengers = 0
    sum_nb_frames = 0
    nb_persons = 0

    for travel in neg_dataset:
        neg_nb_travel_passengers += len(travel.passengersData)
        for passenger in travel.passengersAligned:
            sum_nb_frames += len(passenger)
            nb_persons += 1

    for travel in pos_dataset:
        pos_nb_travel_passengers += len(travel.passengersData)
        for passenger in travel.passengersAligned:
            sum_nb_frames += len(passenger)
            nb_persons += 1

    print('There are ', pos_nb_travel_passengers,
          ' pairs of positive comparison between driver and passenger ')
    print('There are ', neg_nb_travel_passengers,
          ' pairs of negative comparison between driver and passenger ')
    print('Mean number of frames per pair : ', round(sum_nb_frames / nb_persons, 0))


def keep_relevant_cols(dataset, cols_name):
    for travel in dataset:
        i = 0
        passenger: DataFrame
        for passenger in travel.passengersAligned:
            # Drop useless columns for our problem
            travel.driverAligned[i] = travel.driverAligned[i].drop(columns=cols_name)
            travel.passengersAligned[i] = passenger.drop(columns=cols_name)
            i += 1
    return dataset


def truncate_neg_case(dataset):
    for travel in dataset:
        driver = travel.driverData
        for passenger in travel.passengersData:
            # if passenger has more frames, take only the same number of driver's frames
            if (passenger.shape[0] > driver.shape[0]):
                passenger = passenger[:driver.shape[0]]
            else:
                driver = driver[:passenger.shape[0]]

            travel.driverAligned.append(driver)
            travel.passengersAligned.append(passenger)

    return dataset


if __name__ == '__main__':
    # Read all files and return them as list of positive travel usecases and negative travel usecases
    negative_data = read_data(data_negative_cases)
    positive_data = read_data(data_positive_cases)

    # Align all passengers data with driver data based on the nearest timestamp just for positive cases
    # It is important to give positive data pair which are captured each frame at the same time for our futur models
    positive_data_aligned = get_aligned_data(positive_data)

    # No need to align here, just to truncate the longest one to make match the number of pair frame
    negative_data_aligned = truncate_neg_case(negative_data)

    # remove all except Gyro
    #cols_name = ['timestamp', 'driver', 'travellers', 'usecase', 'Accelerometer_x', 'Accelerometer_y', 'Accelerometer_z', 'Magnetometer_x', 'Magnetometer_y', 'Magnetometer_z', 'Magnitude']

    # remove only Gyro
    cols_name = ['timestamp', 'driver', 'travellers', 'usecase', 'Gyroscope_x', 'Gyroscope_y', 'Gyroscope_z']

    # keep all sensors
    # cols_name=['timestamp', 'driver', 'travellers', 'usecase']

    # Remove useless column like (timestamp, driver, travellers and usecase)
    positive_dataset = keep_relevant_cols(positive_data_aligned, cols_name)
    negative_dataset = keep_relevant_cols(negative_data_aligned, cols_name)


    # Save aligned data
    save_data(data_path, positive_dataset, negative_dataset, sub_path=aligned_dir, type='aligned')

    # Print some dataset informations
    print_dataset_informations(positive_dataset, negative_dataset)
