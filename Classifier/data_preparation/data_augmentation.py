import numpy as np
import pandas as pd
from data_preparation.utils import read_data
import config
from itertools import permutations
from data_preparation.utils import save_dataframe
import os

# CONSTANTS
data_path = config.constants.get('DATA_PATH')
data_positive_cases_path = config.constants.get("ALIGNED_POSITIVE_CASES_PATH")
data_negative_cases_path = config.constants.get("ALIGNED_NEGATIVE_CASES_PATH")
# CONSTANTS


def data_permutation(dataset, permutation_percentage):
    permutation_driver_list = []
    permutation_passenger_list = []

    nb_cols = dataset[0].driverData.shape[1]
    cols_name = dataset[0].driverData.columns
    lst_last_idx = np.arange(6, nb_cols, 1)

    # columns indexes to permute for accelerometer sensor
    idx_to_permute_accelerometer = [0, 1, 2]
    perms_accelerometer = set(permutations(idx_to_permute_accelerometer))

    # columns indexes to permute for magnetometer sensor
    # (use the perms_accelerometer previously generated and add +3 to each index --> the result will do the same
    # permutations as accelerometer and make same transformation in the two sensors)
    perms_magnetometer = perms_accelerometer
    perms_magnetometer = list(perms_magnetometer)
    i = 0
    for p_list in perms_magnetometer:
        p_list = list(p_list)
        perms_magnetometer[i] = (p_list[0] + 3, p_list[1] + 3, p_list[2] + 3)
        i += 1

    for travel in dataset:
        driver_data = np.array(travel.driverData)
        passenger_data = np.array(travel.passengersData[0])
        for perm_accelero, perm_magneto in zip(perms_accelerometer, perms_magnetometer):
            cols_idx = np.concatenate((perm_accelero, perm_magneto, lst_last_idx))

            tmp_driver = driver_data[0:int(driver_data.shape[0]*permutation_percentage), cols_idx]
            tmp_driver = np.vstack((tmp_driver,
                                    driver_data[int(driver_data.shape[0] * permutation_percentage):driver_data.shape[0]]))

            tmp_passenger = passenger_data[0:int(passenger_data.shape[0]*permutation_percentage), cols_idx]
            tmp_passenger = np.vstack((tmp_passenger,
                                       passenger_data[int(passenger_data.shape[0] * permutation_percentage):passenger_data.shape[0]]))

            tmp_driver = pd.DataFrame(tmp_driver)
            tmp_passenger = pd.DataFrame(tmp_passenger)

            tmp_driver.columns = cols_name
            tmp_passenger.columns = cols_name

            permutation_driver_list.append(tmp_driver)
            permutation_passenger_list.append(tmp_passenger)

    return permutation_driver_list, permutation_passenger_list


def save(case_type, directory_name, driver_list, passenger_list):
    dir_name = 0
    for i in range(len(driver_list)):
        save_dataframe(os.path.join(data_path, directory_name, case_type, str(dir_name)), driver_list[i], 'driver')
        save_dataframe(os.path.join(data_path, directory_name, case_type, str(dir_name)), passenger_list[i], 'passenger')
        dir_name += 1
    print('')


if __name__ == '__main__':
    # Read aligned dataset files for both positive and negative cases
    positive_dataset = read_data(data_positive_cases_path)
    negative_dataset = read_data(data_negative_cases_path)

    # Permutate accelerometer and magnetomerer axis (all combinations inside each sensor) for 100% of training data
    positive_driver_list, positive_passenger_list = data_permutation(positive_dataset, config.constants.get('SPLIT_RATIO'))
    negative_driver_list, negative_passenger_list = data_permutation(negative_dataset, config.constants.get('SPLIT_RATIO'))

    # save data
    save('positive_usecases', 'aligned_permutation_100', positive_driver_list, positive_passenger_list)
    save('negative_usecases', 'aligned_permutation_100', negative_driver_list, negative_passenger_list)

    # Permutate accelerometer and magnetometer axis (all combinations inside each sensor) for only first 20% of data
    positive_driver_list, positive_passenger_list = data_permutation(positive_dataset, 0.2)
    negative_driver_list, negative_passenger_list = data_permutation(negative_dataset, 0.2)

    # save data
    save('positive_usecases', 'aligned_permutation_20', positive_driver_list, positive_passenger_list)
    save('negative_usecases', 'aligned_permutation_20', negative_driver_list, negative_passenger_list)
