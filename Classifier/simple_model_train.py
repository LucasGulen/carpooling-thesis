from sklearn.exceptions import ConvergenceWarning
from sklearn.utils._testing import ignore_warnings

from Classifier.models.regression import Regression
import Classifier.config as config
from Classifier.models.svm import SVM
from Classifier.utils import read_train_val_test_data, get_best_f1_score, save_f1_score
from Classifier.data_preparation.utils import save_dataframe
import optuna
from optuna import Trial
import os
from pathlib import Path
import pickle

# CONSTANTS
# Original data prepared in windows and merged between driver and passenger base path
original_path_only_accelero = config.constants.get('ORIGINAL_BASE_PATH_WINDOWS_ONLY_ACCELERO')
original_path_accelero_magneto = config.constants.get('ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO')

# Original data prepared in windows and merged between driver and passenger (Only accelero)
original_data_positive_only_accelero = config.constants.get("ORIGINAL_DATA_WINDOWS_POSITIVE_ONLY_ACCELERO")
original_data_negative_only_accelero = config.constants.get("ORIGINAL_DATA_WINDOWS_NEGATIVE_ONLY_ACCELERO")

# Original data prepared in windows and merged between driver and passenger (accelero + magneto)
original_data_positive_accelero_magneto = config.constants.get("ORIGINAL_DATA_WINDOWS_POSITIVE_ACCELERO_MAGNETO")
original_data_negative_accelero_magneto = config.constants.get("ORIGINAL_DATA_WINDOWS_NEGATIVE_ACCELERO_MAGNETO")

# Data augmented with axis permutations (base path) and merged between driver and passenger
permuted_path_only_accelero = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO')
permuted_path_accelero_magneto = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO')
permuted_path_only_accelero_all = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO_ALL')
permuted_path_accelero_magneto_all = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO_ALL')

# Data augmented with axis permutations (only accelero) and merged between driver and passenger
permuted_data_positive_only_accelero = config.constants.get("PERMUTED_DATA_WINDOW_POSITIVE_ONLY_ACCELERO")
permuted_data_negative_only_accelero = config.constants.get("PERMUTED_DATA_WINDOW_NEGATIVE_ONLY_ACCELERO")

# Data augmented with axis permutations (accelero + magneto) and merged between driver and passenger
permuted_data_positive_accelero_magneto = config.constants.get("PERMUTED_DATA_WINDOW_POSITIVE_ACCELERO_MAGNETO")
permuted_data_negative_accelero_magneto = config.constants.get("PERMUTED_DATA_WINDOW_NEGATIVE_ACCELERO_MAGNETO")
# CONSTANTS


class ObjectiveLogReg(object):
    def __init__(self, train_data, validation_data, test_data, window_size):
        self.train_data = train_data
        self.validation_data = validation_data
        self.test_data = test_data
        self.window_size = window_size

    @ignore_warnings(category=ConvergenceWarning)
    def __call__(self, trial: Trial):
        # C parameter give the regularization strength between 0 and 1 (smaller value, stronger reg)
        c = trial.suggest_loguniform('C', 10e-10, 1)
        # solver parameter specify the solver used by sklearn (check documentation for more info)
        solver = trial.suggest_categorical('solver', ['saga', 'sag', 'newton-cg', 'lbfgs'])

        params = {'C': c, 'solver': solver}

        print('################################')
        print('Logistic model parameters', trial.params)
        print('################################')
        model = Regression(self.train_data, self.validation_data, self.test_data, self.window_size, 'window', params)

        # train it and test it on validation dataset
        model.train()
        model.test(test_mode='validation')

        validation_f1_score = model.get_test_f1_score('validation')

        # save model and precision only if it is better than the previous best one
        if validation_f1_score > get_best_f1_score(os.path.join(Path.cwd(), 'Classifier', 'models',
                                                                  'best_regression_params', "best_f1_score_" + str(self.window_size) + ".txt")):
            save_f1_score(validation_f1_score, os.path.join(Path.cwd(), 'Classifier', 'models', 'best_regression_params',
                                                              "best_f1_score_" + str(self.window_size) + ".txt"))
            # save model
            with open(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_regression_params', 'model_' + str(self.window_size) + '.sav'),
                      'wb') as f:
                pickle.dump(model, f)

        return validation_f1_score


class ObjectiveSVM(object):
    def __init__(self, train_data, validation_data, test_data, window_size):
        self.train_data = train_data
        self.validation_data = validation_data
        self.test_data = test_data
        self.window_size = window_size

    @ignore_warnings(category=ConvergenceWarning)
    def __call__(self, trial: Trial):
        # C parameter give the regularization strength between 0 and 1 (smaller value, stronger reg)
        c = trial.suggest_loguniform('C', 10e-10, 1)
        # solver parameter specify the solver used by sklearn (check documentation for more info)
        kernel = trial.suggest_categorical('kernel', ['rbf', 'sigmoid', 'linear', 'poly'])
        # gamma parameter specify the kernel coefficient
        gamma = trial.suggest_loguniform('gamma', 10e-5, 10e-2)

        params = {'C': c, 'kernel': kernel, 'gamma': gamma}

        print('################################')
        print('SVM model parameters', trial.params)
        print('################################')
        model = SVM(self.train_data, self.validation_data, self.test_data, self.window_size, 'window', params)

        # train it and test it on validation dataset
        model.train()
        model.test(test_mode='validation')

        validation_f1_score = model.get_test_f1_score('validation')

        # save model and precision only if it is better than the previous best one
        if validation_f1_score > get_best_f1_score(os.path.join(Path.cwd(), 'Classifier', 'models',
                                                                  'best_svm_params', "best_f1_score_" + str(self.window_size) + ".txt")):
            save_f1_score(validation_f1_score, os.path.join(Path.cwd(), 'Classifier', 'models', 'best_svm_params',
                                                              "best_f1_score_" + str(self.window_size) + ".txt"))
            # save model
            with open(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_svm_params', 'model' + str(self.window_size) + '.sav'),
                      'wb') as f:
                pickle.dump(model, f)

        return validation_f1_score


def simple_model_train(model_name, model_class_name, objective_class_name, window_size_lst):
    for window_size in window_size_lst:
        optuna.logging.set_verbosity(optuna.logging.WARNING)
        optuna.logging.disable_propagation()
        # Window data already concat between passenger and driver for window size of 20
        positive_data_path = Path(os.path.join(original_path_accelero_magneto, str(window_size),
                          'window_merged', 'positive_usecases'))
        negative_data_path = Path(os.path.join(original_path_accelero_magneto, str(window_size),
                                               'window_merged', 'negative_usecases'))
        train_data, validation_data, test_data = read_train_val_test_data(positive_data_path,
                                                                          negative_data_path)

        sampler = optuna.samplers.TPESampler()
        study = optuna.create_study(direction="maximize", sampler=sampler, study_name='Reg_train',
                                    storage='sqlite:///Classifier/models/best_' + model_name + '_params/' + model_name
                                            + '_' + str(window_size) + '_study.db',
                                    load_if_exists=True)

        objective_class = globals()[objective_class_name]
        study.optimize(objective_class(train_data, validation_data, test_data, window_size), n_trials=10, show_progress_bar=True)
        print('Best f1_score on ', model_name, '(window of ', str(window_size), ')', ' was : ', study.best_value, ' with params : ', study.best_params)
        study_df = study.trials_dataframe()
        save_dataframe(os.path.join(Path.cwd(), 'Classifier', 'models', 'studies'), study_df, model_name + '_' + str(window_size) + '_optuna_study')

    '''
    # Test best params found on test dataset
    print('########################################')
    print('Test best parameters founded on test data...')
    print('########################################')
    model_class = globals()[model_class_name]
    model = model_class(train_data, validation_data, test_data, 20, 'window', study.best_params)

    # train it and test it on validation dataset
    model.train()
    model.test(test_mode='test')

    model_paths = [permuted_path_only_accelero_all, permuted_path_accelero_magneto_all,
                   permuted_path_only_accelero, permuted_path_accelero_magneto,
                   original_path_only_accelero, original_path_accelero_magneto]
    labels = [model_name + '-acc-permuted-100', model_name + '-acc-magn-permuted-100',
              model_name + '-acc-permuted-20', model_name + '-acc-magn-permuted-20',
              model_name + '-acc-original', model_name + '-acc-magn-original']
    for i in range(0, len(model_paths)):
        # Use best parameters for multiple window size
        print('########################################')
        print('Searching best window_size according to best parameters founded before...')
        accuracy_lst = find_best_window_size(model_paths[i], config.constants.get('WINDOW_SIZE_LST'),
                                             study.best_params, model_name)
        print(accuracy_lst)
        # Make a graph with results on multiple window size and precision on validation dataset
        #########################################
        pyplot.title('Accuracy for each window size')
        pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), accuracy_lst, label=labels[i])
        pyplot.legend()

    # pyplot.ylim(top=100, bottom=0)
    pyplot.show()
    '''
