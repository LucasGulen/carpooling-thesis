import os
import sys
sys.path.append('./')

import inquirer

# Define static list of parameters that can be choosen by the user with the prompt
from Classifier import config
from Classifier.nn_train import nn_train
from Classifier.simple_model_train import simple_model_train
from Classifier.test_pre_trained_classifiers import only_classifier_test

model_types = [
    inquirer.List('model_type',
                  message="Which model type do you want to train for the classifier ?",
                  choices=['SIMPLE', 'NN', 'ALL'],
                  ),
]

simple_model_types = [
    inquirer.List('simple_model_type',
                  message="Which simple model type do you want to train for the classifier ?",
                  choices=['Logistic', 'SVM', 'ALL'],
                  ),
]

nn_model_types = [
    inquirer.List('NN_model_type',
                  message="Which NN model type do you want to train for the classifier ?",
                  choices=['CNN', 'LSTM', 'RNN', 'ALL'],
                  ),
]

# User can be choose between training and testing Classifier models
nn_train_or_test_choices = [
    inquirer.List('nn_train_or_test',
                  message="Do you want to train or test Classifiers?",
                  choices=['Train', 'Test'],
                  ),
]

def prompt_choices_to_user():
    nn_train_or_test = inquirer.prompt(nn_train_or_test_choices)["nn_train_or_test"]
    if nn_train_or_test == 'Train':
        model_type = inquirer.prompt(model_types)["model_type"]
        window_size_lst = config.constants.get("WINDOW_SIZE_LST")
        # Compute the simple model choosen by the user
        if model_type == 'SIMPLE':
            simple_type = inquirer.prompt(simple_model_types)["simple_model_type"]
            if simple_type == 'Logistic':
                print('Computing logistic regression model ...')
                simple_model_train('regression', 'Regression', 'ObjectiveLogReg', window_size_lst)
            elif simple_type == 'SVM':
                print('Computing SVM model ...')
                simple_model_train('svm', 'SVM', 'ObjectiveSVM', window_size_lst)
            elif simple_type == 'ALL':
                print('Computing logistic regression model ...')
                simple_model_train('regression', 'Regression', 'ObjectiveLogReg', window_size_lst)
                print('Computing SVM model ...')
                simple_model_train('svm', 'SVM', 'ObjectiveSVM', window_size_lst)
        # Compute the Neural Network model choosen by the user
        elif model_type == 'NN':
            nn_type = inquirer.prompt(nn_model_types)["NN_model_type"]
            if nn_type == 'CNN':
                print('Computing CNN model ...')
                nn_train('CNN', 'cnn', window_size_lst)
            elif nn_type == 'LSTM':
                print('Computing LSTM model ...')
                nn_train('LSTM', 'lstm', window_size_lst)
            elif nn_type == 'RNN':
                print('Computing RNN model ...')
                nn_train('RNN', 'rnn', window_size_lst)
            elif nn_type == 'ALL':
                print('Computing LSTM model ...')
                nn_train('LSTM', 'lstm', window_size_lst)
                print('Computing CNN model ...')
                nn_train('CNN', 'cnn', window_size_lst)
                print('Computing RNN model ...')
                nn_train('RNN', 'rnn', window_size_lst)
        # Compute all models
        elif model_type == 'ALL':
            print('Compute NN and simple models for classifier')
            simple_model_train('regression', 'Regression', 'ObjectiveLogReg', window_size_lst)
            simple_model_train('svm', 'SVM', 'ObjectiveSVM', window_size_lst)
            nn_train('CNN', 'cnn', window_size_lst)
            nn_train('RNN', 'rnn', window_size_lst)
            nn_train('LSTM', 'lstm', window_size_lst)

    elif nn_train_or_test == 'Test':
        only_classifier_test()

if __name__ == '__main__':
    prompt_choices_to_user()


