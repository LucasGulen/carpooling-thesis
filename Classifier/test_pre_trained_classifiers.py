import sklearn
from statsmodels.stats import contingency_tables
from matplotlib import pyplot

import Classifier.config as config
import os
from pathlib import Path
import pickle
import numpy as np

from Classifier.data_preparation.utils import load_cnn_data
from Classifier.models.regression import Regression
from Classifier.models.svm import SVM
from Classifier.utils import read_train_val_test_data
import torch
import warnings
warnings.filterwarnings("ignore")


# CONSTANTS
# Original data prepared in windows and merged between driver and passenger base path
original_path_only_accelero = config.constants.get('ORIGINAL_BASE_PATH_WINDOWS_ONLY_ACCELERO')
original_path_accelero_magneto = config.constants.get('ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO')

# Original data prepared in windows and merged between driver and passenger (Only accelero)
original_data_positive_only_accelero = config.constants.get("ORIGINAL_DATA_WINDOWS_POSITIVE_ONLY_ACCELERO")
original_data_negative_only_accelero = config.constants.get("ORIGINAL_DATA_WINDOWS_NEGATIVE_ONLY_ACCELERO")

# Original data prepared in windows and merged between driver and passenger (accelero + magneto)
original_data_positive_accelero_magneto = config.constants.get("ORIGINAL_DATA_WINDOWS_POSITIVE_ACCELERO_MAGNETO")
original_data_negative_accelero_magneto = config.constants.get("ORIGINAL_DATA_WINDOWS_NEGATIVE_ACCELERO_MAGNETO")

# Data augmented with axis permutations (base path) and merged between driver and passenger
permuted_path_only_accelero = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO')
permuted_path_accelero_magneto = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO')
permuted_path_only_accelero_all = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO_ALL')
permuted_path_accelero_magneto_all = config.constants.get('PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO_ALL')

# Data augmented with axis permutations (only accelero) and merged between driver and passenger
permuted_data_positive_only_accelero = config.constants.get("PERMUTED_DATA_WINDOW_POSITIVE_ONLY_ACCELERO")
permuted_data_negative_only_accelero = config.constants.get("PERMUTED_DATA_WINDOW_NEGATIVE_ONLY_ACCELERO")

# Data augmented with axis permutations (accelero + magneto) and merged between driver and passenger
permuted_data_positive_accelero_magneto = config.constants.get("PERMUTED_DATA_WINDOW_POSITIVE_ACCELERO_MAGNETO")
permuted_data_negative_accelero_magneto = config.constants.get("PERMUTED_DATA_WINDOW_NEGATIVE_ACCELERO_MAGNETO")

# window_size_lst = config.constants.get('WINDOW_SIZE_LST')
window_size_lst = [5]
# CONSTANTS

# McNemar test
def get_mcnemar_table(model_1, model_2) :
    # Case wrong by the model 1 and correct by the model 2
    wrong_1_correct_2 = np.count_nonzero(np.logical_and(model_1 == False, model_2))
    # Case correct by the model 1 and wrong by the model 2
    correct_1_wrong_2 = np.count_nonzero(np.logical_and(model_1, model_2 == False))
    # Case wrong by the model 1 and wrong by the model 2
    wong_1_wrong_2 = np.count_nonzero(np.logical_and(model_1 == False, model_2 == False))
    # Case correct by the model 1 and correct by the model 2
    correct_1_correct_2 = np.count_nonzero(np.logical_and(model_1, model_2))

    theoritical_frequencies = (wrong_1_correct_2 + correct_1_wrong_2) / 2

    p_value = (((wrong_1_correct_2 - theoritical_frequencies) ** 2) / theoritical_frequencies) + (((correct_1_wrong_2 - theoritical_frequencies) ** 2) / theoritical_frequencies)

    return correct_1_correct_2, correct_1_wrong_2, wrong_1_correct_2,wong_1_wrong_2, p_value



def only_classifier_test():
    lst_precision_reg = []
    lst_precision_svm = []
    lst_precision_rnn = []
    lst_precision_lstm = []
    lst_precision_cnn = []
    for window_size in window_size_lst:
        # Load data for regression and svm
        _, _, test_data = read_train_val_test_data(Path(os.path.join(original_path_accelero_magneto, str(window_size),
                                                                     'window_merged', 'positive_usecases')),
                                                   Path(os.path.join(original_path_accelero_magneto, str(window_size),
                                                                     'window_merged', 'negative_usecases')))
        Y_test = test_data[:, test_data.shape[1] - 1]
        X_test = np.delete(test_data, test_data.shape[1] - 1, 1)  # remove last column

        # Compute regression
        regression: Regression = pickle.load(open(os.path.join(Path.cwd(), 'Classifier', 'models',
                                                               'best_regression_params', 'model_' + str(window_size) + '.sav'), 'rb'))
        precision_regression = regression.test(test_mode='test', test_x=X_test, test_y=Y_test)
        lst_precision_reg.append(precision_regression)
        del regression

        # Compute SVM
        svm: SVM = pickle.load(open(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_svm_params', 'model' +
                                                 str(window_size) + '.sav'), 'rb'))
        precision_svm = svm.test(test_mode='test', test_x=X_test, test_y=Y_test)
        lst_precision_svm.append(precision_svm)

        # Load data for rnn and lstm
        positive_data_path = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'positive_usecases'))
        negative_data_path = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'negative_usecases'))

        # Load data for passenger and driver
        _, _ , test_data = read_train_val_test_data(positive_data_path, negative_data_path)
        Y_test = test_data[:, test_data.shape[1] - 1]
        X_test = np.delete(test_data, test_data.shape[1] - 1, 1)  # remove last column
        del svm

        # Compute rnn
        rnn = torch.load(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_rnn_params', 'model_' + str(window_size) + '.pth'))
        rnn.eval()
        X_test = torch.Tensor(X_test).float().view(-1, window_size*7*2, 1).cuda()
        outputs_rnn = rnn.predict(X_test)
        precision_rnn = sklearn.metrics.f1_score(Y_test, outputs_rnn.cpu().numpy(), average='binary') * 100
        # print('RNN f1 score on test data : ', precision_rnn)
        lst_precision_rnn.append(precision_rnn)

        # Compute lstm
        lstm = torch.load(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_lstm_params', 'model_' + str(window_size) + '.pth'))
        lstm.eval()
        outputs_lstm = lstm.predict(X_test)
        precision_lstm = sklearn.metrics.precision_score(Y_test, outputs_lstm.cpu(), average='binary') * 100
        # print('LSTM f1 score on test data : ', precision_lstm)

        lst_precision_lstm.append(precision_lstm)


        m1_m2_c, m1_c_m2_w, m1_w_m2_c, m1_m2_w, p_value = get_mcnemar_table(outputs_lstm.cpu().numpy() == Y_test, outputs_rnn.cpu().numpy() == Y_test)

        c = m1_c_m2_w
        b = m1_w_m2_c

        p = np.power((b - c), 2) / (b+c)
        print(p_value)
        print(p)

        table = np.array([[m1_m2_c, m1_w_m2_c],[m1_c_m2_w, m1_m2_w]])

        print(table)

        print(contingency_tables.mcnemar(table, exact=False, correction=True))

        del lstm
        del rnn

        # Load data for cnn
        cnn_data_path = os.path.join(config.constants.get("CNN_ACCELERO_MAGNETO"), str(window_size))

        X_test = torch.FloatTensor(load_cnn_data(cnn_data_path, 'test')).cuda()
        Y_test = load_cnn_data(cnn_data_path, 'test_y')
        # Compute cnn
        cnn = torch.load(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_cnn_params', 'model_' + str(window_size) + '.pth'))
        cnn.eval()

        outputs = cnn.predict(X_test)
        precision_cnn = sklearn.metrics.f1_score(Y_test, outputs.cpu(), average='binary') * 100
        lst_precision_cnn.append(precision_cnn)

        # Print results
        # print('Precision for window size of ', window_size, ':')
        # print('Logistic : ', precision_regression)
        # print('SVM : ', precision_svm)
        # print('RNN : ', precision_rnn)
        print('LSTM : ', precision_lstm)
        # print('CNN : ', precision_cnn)

    '''
    # Plot result and add it to the same graph with precision at y axis and window_size at x axis
    labels = ['Logistic', 'SVM', 'RNN', 'LSTM', 'CNN']
    pyplot.title('Models precision by window_size')
    pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), lst_precision_reg, label=labels[0])
    pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), lst_precision_svm, label=labels[1])
    pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), lst_precision_rnn, label=labels[2])
    pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), lst_precision_lstm, label=labels[3])
    pyplot.plot(config.constants.get('WINDOW_SIZE_LST'), lst_precision_cnn, label=labels[4])
    pyplot.legend()

    # pyplot.ylim(top=100, bottom=0)
    pyplot.show()
    '''

if __name__ == '__main__':
    only_classifier_test()
