import os
from pathlib import Path

BEST_LSTM_PRECISION = 85
BEST_CNN_PRECISION = 85
BEST_RNN_PRECISION = 98
MIN_MSE_AE = 273.335
# MIN_MSE_AE_CNN = 79.13
MIN_MSE_AE_CNN = 200

constants = {
    'DATA_PATH': os.path.join(Path.cwd(), 'data'),
    'RAW_POSITIVE_CASES': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'raw', 'positive_usecases')),
    'RAW_NEGATIVE_CASES': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'raw', 'negative_usecases')),
    'ALIGNED_POSITIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned', 'positive_usecases')),
    'ALIGNED_NEGATIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned', 'negative_usecases')),

    'ALIGNED_AUGMENTED_100_POSITIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned_permutation_100', 'positive_usecases')),
    'ALIGNED_AUGMENTED_100_NEGATIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned_permutation_100', 'negative_usecases')),

    'ALIGNED_AUGMENTED_20_POSITIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned_permutation_20', 'positive_usecases')),
    'ALIGNED_AUGMENTED_20_NEGATIVE_CASES_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'aligned_permutation_20', 'negative_usecases')),

    'ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                     'windows')),

    'ORIGINAL_DATA_WINDOWS_POSITIVE_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'windows',
                                                                         '20', 'window_merged', 'positive_usecases')),
    'ORIGINAL_DATA_WINDOWS_NEGATIVE_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'), 'windows',
                                                                         '20', 'window_merged', 'negative_usecases')),

    'ORIGINAL_BASE_PATH_WINDOWS_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                  'windows_accelero')),

    'ORIGINAL_DATA_WINDOWS_POSITIVE_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                      'windows_accelero', '20', 'window_merged', 'positive_usecases')),
    'ORIGINAL_DATA_WINDOWS_NEGATIVE_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                      'windows_accelero', '20', 'window_merged', 'negative_usecases')),

    'PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                  'windows_augmented_20_accelero')),

    'PERMUTED_BASE_PATH_WINDOWS_ONLY_ACCELERO_ALL': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                  'windows_augmented_100_accelero')),

    'PERMUTED_DATA_WINDOW_POSITIVE_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                     'windows_augmented_20_accelero', '20',
                                                                     'window_merged', 'positive_usecases')),

    'PERMUTED_DATA_WINDOW_NEGATIVE_ONLY_ACCELERO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                     'windows_augmented_20_accelero', '20',
                                                                     'window_merged', 'negative_usecases')),

    'PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                     'windows_augmented_20')),

    'PERMUTED_BASE_PATH_WINDOWS_ACCELERO_MAGNETO_ALL': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                     'windows_augmented_100')),

    'PERMUTED_DATA_WINDOW_POSITIVE_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                        'windows_augmented_20', '20',
                                                                        'window_merged', 'positive_usecases')),
    'PERMUTED_DATA_WINDOW_NEGATIVE_ACCELERO_MAGNETO': Path(os.path.join(os.path.join(Path.cwd(), 'data'),
                                                                        'windows_augmented_20', '20',
                                                                        'window_merged', 'negative_usecases')),

    'BEST_REG_PARAMS_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'Classifier'), 'models', 'best_reg_params')),
    'BEST_SVM_PARAMS_PATH': Path(os.path.join(os.path.join(Path.cwd(), 'Classifier'), 'models', 'best_svm_params')),

    'CNN_ACCELERO_MAGNETO': Path(os.path.join(Path.cwd(), 'data', 'cnn_windows')),

    'ALIGNED_DIRECTORY': 'aligned',

    'CONCAT_COL_NAMES_ALL': ['Acc_x', 'Acc_x1', 'Acc_y', 'Acc_y1', 'Acc_z', 'Acc_z1',
                             'Gyro_x', 'Gyro_x1', 'Gyro_y', 'Gyro_y1', 'Gyro_z', 'Gyro_z1',
                             'Magne_x', 'Magne_x1', 'Magne_y', 'Magne_y1', 'Magne_z', 'Magne_z1', 'Magni', 'Magni_1'],

    'CONCAT_COL_NAMES': ['Acc_x', 'Acc_x1', 'Acc_y', 'Acc_y1', 'Acc_z', 'Acc_z1', 'Magne_x', 'Magne_x1',
                         'Magne_y', 'Magne_y1', 'Magne_z', 'Magne_z1', 'Magni', 'Magni_1'],

    'CONCAT_COL_NAMES_ONLY_GYRO': ['Gyro_x', 'Gyro_x1', 'Gyro_y', 'Gyro_y1', 'Gyro_z', 'Gyro_z1'],

    'WINDOW_SIZE': 20,

    'SPLIT_RATIO': 0.7,

    #'WINDOW_SIZE_LST': [20, 40, 60, 120],
    'WINDOW_SIZE_LST': [5, 8, 13, 21, 34, 55, 89, 144],
    # 'WINDOW_SIZE_LST': [34, 55, 89, 144],

    'COL_NAMES_WINDOW_CONCAT_ALL': ['Acc_x_d', 'Acc_x_d1', 'Acc_x_d2', 'Acc_y_d', 'Acc_y_d1', 'Acc_y_d2', 'Acc_z_d',
                                    'Acc_z_d1', 'Acc_z_d2', 'Gyro_x_d', 'Gyro_x_d1', 'Gyro_x_d2', 'Gyro_y_d',
                                    'Gyro_y_d1', 'Gyro_y_d2', 'Gyro_z_d', 'Gyro_z_d1', 'Gyro_z_d2',
                                    'Magne_x_d', 'Magne_x_d1', 'Magne_x_d2', 'Magne_y_d',
                                    'Magne_y_d1', 'Magne_y_d2', 'Magne_z_d', 'Magne_z_d1', 'Magne_z_d2',
                                    'Magni_d', 'Magni_d1', 'Magni_d2'],

    'COL_NAMES_WINDOW_CONCAT': ['Acc_x_d', 'Acc_x_d1', 'Acc_x_d2', 'Acc_y_d', 'Acc_y_d1', 'Acc_y_d2', 'Acc_z_d', 'Acc_z_d1', 'Acc_z_d2',
                                'Magne_x_d', 'Magne_x_d1', 'Magne_x_d2', 'Magne_y_d', 'Magne_y_d1', 'Magne_y_d2', 'Magne_z_d', 'Magne_z_d1', 'Magne_z_d2',
                                'Magni_d', 'Magni_d1', 'Magni_d2'],

    'COL_NAMES_WINDOW_CONCAT_ONLY_GYRO': ['Gyro_x_d', 'Gyro_x_d1', 'Gyro_x_d2', 'Gyro_y_d', 'Gyro_y_d1', 'Gyro_y_d2', 'Gyro_z_d',
                                          'Gyro_z_d1', 'Gyro_z_d2'],

    'COL_NAMES_WINDOW_CONCAT_MERGED_ALL': ['Acc_x_d', 'Acc_x_p', 'Acc_x_d1', 'Acc_x_p1', 'Acc_x_d2', 'Acc_x_p2',
                                           'Acc_y_d', 'Acc_y_p', 'Acc_y_d1', 'Acc_y_p1', 'Acc_y_d2', 'Acc_y_p2',
                                           'Acc_z_d', 'Acc_z_p', 'Acc_z_d1', 'Acc_z_p1', 'Acc_z_d2', 'Acc_z_p2',
                                           'Gyro_x_d', 'Gyro_x_p', 'Gyro_x_d1', 'Gyro_x_p1', 'Gyro_x_d2', 'Gyro_x_p2',
                                           'Gyro_y_d', 'Gyro_y_p', 'Gyro_y_d1', 'Gyro_y_p1', 'Gyro_y_d2', 'Gyro_y_p2',
                                           'Gyro_z_d', 'Gyro_z_p', 'Gyro_z_d1', 'Gyro_z_p1', 'Gyro_z_d2', 'Gyro_z_p2',
                                           'Magne_x_d', 'Magne_x_p', 'Magne_x_d1', 'Magne_x_p1', 'Magne_x_d2',
                                           'Magne_x_p2', 'Magne_y_d', 'Magne_y_p', 'Magne_y_d1', 'Magne_y_p1',
                                           'Magne_y_d2', 'Magne_y_p2', 'Magne_z_d', 'Magne_z_p', 'Magne_z_d1',
                                           'Magne_z_p1', 'Magne_z_d2', 'Magne_z_p2', 'Magni_d', 'Magni_p', 'Magni_d1',
                                           'Magni_p1', 'Magni_d2', 'Magni_p2'],

    'COL_NAMES_WINDOW_CONCAT_MERGED': ['Acc_x_d', 'Acc_x_p', 'Acc_x_d1', 'Acc_x_p1', 'Acc_x_d2', 'Acc_x_p2',
                                       'Acc_y_d', 'Acc_y_p', 'Acc_y_d1', 'Acc_y_p1', 'Acc_y_d2', 'Acc_y_p2',
                                       'Acc_z_d', 'Acc_z_p', 'Acc_z_d1', 'Acc_z_p1', 'Acc_z_d2', 'Acc_z_p2',
                                       'Magne_x_d', 'Magne_x_p', 'Magne_x_d1', 'Magne_x_p1', 'Magne_x_d2',
                                       'Magne_x_p2', 'Magne_y_d', 'Magne_y_p', 'Magne_y_d1', 'Magne_y_p1',
                                       'Magne_y_d2', 'Magne_y_p2', 'Magne_z_d', 'Magne_z_p', 'Magne_z_d1',
                                       'Magne_z_p1', 'Magne_z_d2', 'Magne_z_p2', 'Magni_d', 'Magni_p', 'Magni_d1',
                                       'Magni_p1', 'Magni_d2', 'Magni_p2'],

    'COL_NAMES_WINDOW_CONCAT_MERGED_ONLY_GYRO': ['Gyro_x_d', 'Gyro_x_p', 'Gyro_x_d1', 'Gyro_x_p1', 'Gyro_x_d2', 'Gyro_x_p2',
                                       'Gyro_y_d', 'Gyro_y_p', 'Gyro_y_d1', 'Gyro_y_p1', 'Gyro_y_d2', 'Gyro_y_p2',
                                       'Gyro_z_d', 'Gyro_z_p', 'Gyro_z_d1', 'Gyro_z_p1', 'Gyro_z_d2', 'Gyro_z_p2']

    # 'CONCAT_COL_NAMES': ['Acc_x', 'Acc_x1', 'Acc_y', 'Acc_y1', 'Acc_z', 'Acc_z1', 'Gyr_x',
    #                    'Gyr_x1', 'Gyr_y', 'Gyr_y1', 'Gyr_z', 'Gyr_z1', 'Magne_x', 'Magne_x1',
    #                   'Magne_y', 'Magne_y1', 'Magne_z', 'Magne_z1', 'Magni', 'Magni_1']

}
