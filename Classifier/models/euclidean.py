from metier.travel import Travel
import numpy as np


def euclidean_distance(pos_data: [Travel], neg_data: [Travel]):
    dataset_driver = np.empty((0, pos_data[0].driverData.shape[1]))
    dataset_passengers = np.empty((0, pos_data[0].passengersData[0].shape[1]))

    for travel in pos_data:
        dataset_driver = np.vstack((dataset_driver, travel.driverData))
        dataset_passengers = np.vstack((dataset_passengers, travel.passengersData[0]))

    for travel in neg_data:
        dataset_driver = np.vstack((dataset_driver, travel.driverData))
        dataset_passengers = np.vstack((dataset_passengers, travel.passengersData[0]))

    distance = np.sqrt(np.sum(np.square(dataset_driver - dataset_passengers), axis=1))
    print("Euclidean distance from driver frames to passenger frames : ", distance)

euclidean_distance(positive_data, negative_data)
