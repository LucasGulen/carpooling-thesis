from abc import ABC, abstractmethod

from pandas import DataFrame

from metier.travel import Travel


class Model(ABC):
    def __init__(self, train: DataFrame, validation: DataFrame,  test: DataFrame, window_size: int, model_type, parameters: dict):
        self.train_data = train
        self.validation_data = validation
        self.test_data = test
        self.test_y = None
        self.window_size = window_size
        self.model_type = model_type
        self.parameters = parameters
        self.prepare_data()

    @abstractmethod
    def prepare_data(self):
        pass

    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def test(self):
        pass

