import sklearn

from Classifier.models.model import Model
from sklearn.linear_model import LinearRegression, LogisticRegression
import numpy as np


class Regression(Model):

    def __prepare_data(self):
        # Remove y from dataset and store it in another variable (last column)
        train_y = self.train_data[:, self.train_data.shape[1] - 1]
        train_data = np.delete(self.train_data, self.train_data.shape[1] - 1, 1)  # remove last column

        validation_y = self.validation_data[:, self.validation_data.shape[1] - 1]
        validation_data = np.delete(self.validation_data, self.validation_data.shape[1] - 1, 1)  # remove last column

        test_y = self.test_data[:, self.test_data.shape[1] - 1]
        test_data = np.delete(self.test_data, self.test_data.shape[1] - 1, 1)  # remove last column

        self.train_x = train_data
        self.train_y = train_y

        self.validation_x = validation_data
        self.validation_y = validation_y

        self.test_x = test_data
        self.test_y = test_y

    def get_test_f1_score(self, test_mode):
        if test_mode == 'validation':
            # Calculate F1 score, recall and precision
            f1_score = sklearn.metrics.f1_score(self.validation_y, self.validation_preds, average='binary') * 100
            recall = sklearn.metrics.recall_score(self.validation_y, self.validation_preds, average='binary') * 100
            precision = sklearn.metrics.precision_score(self.validation_y, self.validation_preds, average='binary') * 100
            return round(f1_score, 6)
        else:
            # Calculate F1 score, recall and precision
            f1_score = sklearn.metrics.f1_score(self.test_y, self.test_preds, average='binary') * 100
            recall = sklearn.metrics.recall_score(self.test_y, self.test_preds, average='binary') * 100
            precision = sklearn.metrics.precision_score(self.test_y, self.test_preds, average='binary') * 100
            return round(f1_score, 6)

    def __statistics(self, test_mode):
        if test_mode == 'validation':
            precision = self.get_test_f1_score(test_mode)
            print('F1_score on validation data : ', precision, '%')
        else:
            precision = self.get_test_f1_score(test_mode)
            # print('Logistic F1_score on test data : ', precision, '%')
        return precision

    def prepare_data(self):
        # Extract y from the dataset
        self.__prepare_data()


    def train(self):
        self.model = None
        self.model = LogisticRegression(
            C=self.parameters.get('C'),
            solver=self.parameters.get('solver')
        ).fit(self.train_x, self.train_y)

    def test(self, test_mode, test_x=None, test_y=None):
        if test_mode == 'validation':
            preds = self.model.predict(self.validation_x)
            self.validation_preds = preds.astype(int)
        else:
            self.test_y = test_y
            preds = self.model.predict(self.test_x)
            self.test_preds = preds.astype(int)
        return self.__statistics(test_mode)

    def weights(self):
        return self.model.coef_
