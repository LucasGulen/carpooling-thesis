from torch import *
from torch import nn
from torch.nn import *
from optuna import Trial
import torch

class CNN(nn.Module):
    def __init__(self, input_channels, output_dim, trial: Trial):
        super().__init__()

        self.layers = []

        # We optimize the number of layers, hidden untis in each layer and drouputs.
        n_layers = trial.suggest_int("n_layers", 1, 10)
        output_channels = trial.suggest_int("n_output_channels", 1, 64)

        for i in range(n_layers):
            drop = trial.suggest_uniform("dropout", 0.1, 0.5)

            self.layers.append(nn.Conv2d(input_channels, output_channels, kernel_size=(2, 3)))
            self.layers.append(BatchNorm2d(output_channels))
            self.layers.append(ReLU(inplace=True))
            self.layers.append(Dropout(drop))
            self.layers.append(AdaptiveMaxPool2d((2, 40)))
            input_channels = output_channels

        self.cnn_layers = Sequential(*self.layers)

        self.linear_layers = Sequential(
            Linear(output_channels * 2 * 40, output_dim),
        )

    # Defining the forward pass
    def forward(self, x):
        x = self.cnn_layers(x)
        x = x.view(x.size(0), -1)
        x = self.linear_layers(x)
        return x

    def predict(self, x):
        outputs = self.forward(x)
        outputs = torch.sigmoid(outputs.data)
        return torch.round(outputs).view(-1)
