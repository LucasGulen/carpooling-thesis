import torch.nn as nn
import torch
from torch.autograd import Variable


class RNN(nn.Module):
    def __init__(self, input_dim, output_dim, model_params):
        super(RNN, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.params = model_params

        self.rnn = nn.RNN(self.input_dim, **model_params, batch_first=True, nonlinearity='relu')

        self.fc = nn.Linear(model_params['hidden_size'], self.output_dim)

    def forward(self, X):
        # Initialize hidden state with zeros
        # (num_layers, batch_size, hidden_dim)
        h0 = torch.zeros(self.params['num_layers'], X.size(0), self.params['hidden_size']).cuda()
        # We need to detach the hidden state to prevent exploding/vanishing gradients
        # This is part of truncated backpropagation through time (BPTT)
        out, hn = self.rnn(X, h0.detach())

        out = self.fc(out[:, -1, :]).cuda()
        return out

    def predict(self, x):
        outputs = self.forward(x)
        outputs = torch.sigmoid(outputs.data)
        return torch.round(outputs).view(-1)
