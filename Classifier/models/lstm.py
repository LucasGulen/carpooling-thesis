import torch
from torch import nn


class LSTM(nn.Module):
    def __init__(self, input_dim, output_dim, params):
        super().__init__()
        self.hidden_dim = params['hidden_size']
        self.layer_dim = params['num_layers']
        self.lstm = nn.LSTM(input_dim, **params, batch_first=True)
        self.fc = nn.Linear(self.hidden_dim, output_dim)

    def forward(self, x):
        # shape of x is (batch_size, seq_length, input_size)
        h0, c0 = self.init_hidden(x)
        out, (hn, cn) = self.lstm(x, (h0, c0))
        out = self.fc(out[:, -1, :]).cuda()
        return out

    def init_hidden(self, x):
        # Pytorch documentation says that shape of h0 and c0 is (num_layers x batch_size x hidden_size)
        # It's common to initialize them to set of zero
        h0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).cuda()
        c0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).cuda()
        return h0, c0

    def predict(self, x):
        outputs = self.forward(x)
        outputs = torch.sigmoid(outputs.data)
        return torch.round(outputs).view(-1)
