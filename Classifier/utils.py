import numpy as np
import torch
import re
from Classifier.data_preparation.utils import read_data
from Classifier.models.regression import Regression
from Classifier.models.svm import SVM
import os
from pathlib import Path


def get_best_f1_score(path):
    if not os.path.exists(path):
        f1_score_file = open(path, 'w+')
        f1_score_file.write('0')
        f1_score_file.close()
        return 0
    else:
        with open(path, "r+") as f1_score_file:
            best_f1_score = float(f1_score_file.read())
    return best_f1_score

# Save given precision only if it's better than the best one previously stored
def save_f1_score(f1_score, path):
    if not os.path.exists(path):
        f1_score_file = open(path, 'w+')
        f1_score_file.write('0')
        f1_score_file.close()
    else:
        with open(path, "r+") as best_f1_score_file:
            best_f1_score = best_f1_score_file.read()
            if f1_score > float(best_f1_score):
                replace_txt = re.sub(best_f1_score, str(f1_score), best_f1_score)
                best_f1_score_file.seek(0)
                best_f1_score_file.write(replace_txt)
                best_f1_score_file.truncate()


def prepare_data_loader(batch_size, train_data, validation_data, test_data):
    # Extract label and delete into x features matrix
    train_y = train_data[:, train_data.shape[1] - 1]
    train = np.delete(train_data, train_data.shape[1] - 1, 1)

    val_y = validation_data[:, validation_data.shape[1] - 1]
    validation = np.delete(validation_data, validation_data.shape[1] - 1, 1)

    test_y = test_data[:, test_data.shape[1] - 1]
    test = np.delete(test_data, test_data.shape[1] - 1, 1)

    train_lst = []
    for i in range(len(train)):
        train_lst.append([train[i], train_y[i]])

    validation_lst = []
    for i in range(len(validation)):
        validation_lst.append([validation[i], val_y[i]])

    test_lst = []
    for i in range(len(test)):
        test_lst.append([test[i], test_y[i]])

    # prepare data in batch (needed by pytorch)
    train_loader = torch.utils.data.DataLoader(train_lst, batch_size=batch_size,
                                               shuffle=True, num_workers=0, drop_last=True, pin_memory=True)

    validation_loader = torch.utils.data.DataLoader(validation_lst, batch_size=batch_size,
                                                    shuffle=True, num_workers=0, drop_last=True, pin_memory=True)

    test_loader = torch.utils.data.DataLoader(test_lst, batch_size=batch_size,
                                              shuffle=True, num_workers=0, drop_last=True, pin_memory=True)

    return train_loader, validation_loader, test_loader


def prepare_cnn_data_loader(batch_size, train, train_y, validation, validation_y, test, test_y):
    train_lst = []
    for i in range(len(train)):
        train_lst.append([train[i, :, :, :], train_y[i]])

    validation_lst = []
    for i in range(len(validation)):
        validation_lst.append([validation[i, :, :, :], validation_y[i]])

    test_lst = []
    for i in range(len(test)):
        test_lst.append([test[i, :, :, :], test_y[i]])


    # prepare data in batch (needed by pytorch)
    train_loader = torch.utils.data.DataLoader(train_lst, batch_size=batch_size,
                                               shuffle=True, num_workers=1, drop_last=True, pin_memory=True)

    validation_loader = torch.utils.data.DataLoader(validation_lst, batch_size=batch_size,
                                                    shuffle=True, num_workers=1, drop_last=True, pin_memory=True)

    test_loader = torch.utils.data.DataLoader(test_lst, batch_size=batch_size,
                                              shuffle=True, num_workers=1, drop_last=True, pin_memory=True)

    return train_loader, validation_loader, test_loader

def read_train_val_test_data(path_positive: str, path_negative: str):
    positive_train, positive_validation, positive_test = read_data(path_positive, True)
    negative_train, negative_validation, negative_test = read_data(path_negative, True)

    train = np.vstack((positive_train, negative_train))
    validation = np.vstack((positive_validation, negative_validation))
    test = np.vstack((positive_test, negative_test))

    return train, validation, test


def find_best_window_size(base_path, lst_window_size, best_params, model_type):
    accuracy_lst = []
    # Loop over each window size contained in list given in parameters
    for window_size in lst_window_size:
        # Load data for the current window_size
        original_positive_path_only_accelero = Path(os.path.join(base_path, str(window_size), 'window_merged', 'positive_usecases'))
        original_negative_path_only_accelero = Path(os.path.join(base_path, str(window_size), 'window_merged', 'negative_usecases'))
        train, val, test = read_train_val_test_data(original_positive_path_only_accelero,
                                                    original_negative_path_only_accelero)

        if model_type == 'regression':
            # Create logistic regression with the current train, validation and test dataset
            model = Regression(train, val, test, window_size, 'window', best_params)
        elif model_type == 'svm':
            model = SVM(train, val, test, window_size, 'window', best_params)

        # train it and test it on validation dataset
        model.train()
        model.test(test_mode='validation')
        # Store result and window size in list
        accuracy_lst.append(model.get_test_precision(test_mode='validation'))

    return accuracy_lst
