from pathlib import Path
import numpy as np
import optuna
import pandas as pd
import torch
from torch.autograd import Variable
import Classifier.config as config
from Classifier.models.cnn import CNN
from Classifier.models.lstm import LSTM
from Classifier.models.rnn import RNN
from Classifier.data_preparation.utils import load_cnn_data, save_dataframe
from torch import *
import torch.nn as nn
import torch.optim as optim
from Classifier.utils import prepare_data_loader, prepare_cnn_data_loader, read_train_val_test_data, get_best_f1_score, save_f1_score
import os
from optuna import *
import sklearn

# global fixed parameters
OUTPUT_DIM = 1
N_EPOCHS = 50
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.cuda.max_memory_allocated()
base_path = os.path.join(Path.cwd(), 'Classifier', 'models')


class ObjectiveCNN(object):
    def __init__(self, train, train_y, val, val_y, test, test_y, window_size):
        self.train = train
        self.train_y = train_y
        self.validation = val
        self.validation_y = val_y
        self.test = test
        self.test_y = test_y
        self.window_size = window_size

    def __call__(self, trial: Trial):
        input_dim = 3
        output_dim = 1
        batch_size = trial.suggest_int('batch_size', 32, 256, step=32)

        train_loader, validation_loader, test_loader = prepare_cnn_data_loader(batch_size, self.train, self.train_y, self.validation,
                                                                               self.validation_y, self.test, self.test_y)
        torch.cuda.empty_cache()
        model = CNN(input_dim, output_dim, trial)
        model.cuda()

        criterion = nn.BCEWithLogitsLoss()

        learning_rate = trial.suggest_loguniform('lr', 1e-10, 1e-2)
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)

        loss_list = []
        accuracy_list = []
        cpt_early_stop = 0
        max_f1_score = 0

        print('################################')
        print('CNN training ', '(window_size of ', str(self.window_size), ') with params : ', trial.params)
        print('################################')
        for epoch in range(N_EPOCHS):  # loop over the dataset multiple times
            model.train()

            # TRAINING ROUND
            for train_inputs, labels in train_loader:
                # zero the parameter gradients
                optimizer.zero_grad()

                inputs = train_inputs.float()

                inputs, labels = inputs.to(DEVICE), labels.to(device=DEVICE, dtype=torch.float)

                # forward + backward + optimize
                outputs = model(inputs)

                loss = criterion(outputs.view(-1), labels)
                loss.cuda()
                loss.backward()
                with torch.no_grad():
                    optimizer.step()

            # Calculate accuracy on validation set after each epoch
            with torch.set_grad_enabled(False):
                preds = model.predict(torch.Tensor(self.validation).cuda()).cpu()

                # Calculate F1 score, recall and precision
                f1_score = sklearn.metrics.f1_score(self.validation_y, preds, average='binary') * 100
                recall = sklearn.metrics.recall_score(self.validation_y, preds, average='binary') * 100
                precision = sklearn.metrics.precision_score(self.validation_y, preds, average='binary') * 100

                # Early stopping if validation accuracy didn't increase in last 10 epochs
                if f1_score < max_f1_score:
                    cpt_early_stop += 1
                    if cpt_early_stop == 10:
                        return max_f1_score
                elif f1_score > max_f1_score:
                    max_f1_score = f1_score
                    cpt_early_stop = 0
                    if max_f1_score > get_best_f1_score(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_cnn_params',
                                                                 "best_f1_score__" + str(self.window_size) + ".txt")):
                        # Save best precision
                        save_f1_score(f1_score, os.path.join(Path.cwd(), 'Classifier', 'models', 'best_cnn_params',
                                                               "best_f1_score" + str(self.window_size) + ".txt"))
                        # Save best model
                        torch.save(model, os.path.join(Path.cwd(), 'Classifier', 'models',
                                                                    'best_cnn_params', 'model_' + str(self.window_size) + '.pth'))

                # store loss and precision (can be useful for plot)
                loss_list.append(loss.data)
                accuracy_list.append(f1_score)
                # Print Loss on train and accuracy on test each epoch
                print(
                    'Epoch: {}  Loss on train: {}  F1_score on validation set: {} %'.format(epoch + 1, loss.data.item(),
                                                                                            f1_score))

        # return loss_list, accuracy_list
        return np.max(accuracy_list)


class ObjectiveRecurrentNetwork(object):

    def __init__(self, train, validation, test, model_name, model_class_name, window_size):
        self.train_data = train
        self.validation_data = validation
        self.test_data = test
        self.model_name = model_name
        self.model_class_name = model_class_name
        self.window_size = window_size
        # 7 features * window_size * 2(passenger + driver)
        self.SEQ_DIM = window_size * 7 * 2
        self.INPUTS_DIM = 1

    def __call__(self, trial: Trial):
        batch_size = trial.suggest_int('batch_size', 1024, 2048, step=64)
        trial.suggest_int('hidden_size', 16, 64)
        trial.suggest_int('num_layers', 1, 2, 3)
        learning_rate = trial.suggest_loguniform('lr', 1e-10, 1e-2)

        train_loader, validation_loader, test_loader = prepare_data_loader(batch_size, self.train_data, self.validation_data,
                                                                           self.test_data)

        model_class = globals()[self.model_class_name]
        model_params = {key: value for key, value in trial.params.items() if
                        key != 'batch_size' and key != 'lr'}
        model = model_class(self.INPUTS_DIM, OUTPUT_DIM, model_params)
        model.to(DEVICE)

        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)

        loss_list = []
        accuracy_list = []
        max_curr_f1 = 0
        cpt_early_stop = 0
        found_best_model = False
        print('################################')
        print(self.model_name, ' model parameters', trial.params, ' (window size of ', str(self.window_size), ')')
        print('################################')
        for epoch in range(N_EPOCHS):  # loop over the dataset multiple times
            torch.cuda.empty_cache()
            model.train()

            # TRAINING ROUND
            for train_inputs, labels in train_loader:
                # zero the parameter gradients
                optimizer.zero_grad()

                inputs = train_inputs.float()
                inputs = inputs.view(-1, self.SEQ_DIM, self.INPUTS_DIM)

                inputs, labels = inputs.to(DEVICE), labels.to(device=DEVICE, dtype=torch.float32)

                # forward + backward + optimize
                outputs = model(inputs)

                loss = criterion(outputs.view(-1), labels)
                try:
                    loss.backward()
                    optimizer.step()
                except:
                    return 0

            # Calculate accuracy on validation set after each epoch
            with torch.set_grad_enabled(False):
                true_labels_lst = []
                preds_lst = []
                # Iterate through test dataset
                for val_inputs, val_labels in validation_loader:
                    val_inputs = Variable(val_inputs.view(-1, self.SEQ_DIM, self.INPUTS_DIM).float())
                    val_inputs, labels = val_inputs.cuda(), val_labels.to(device=DEVICE, dtype=torch.int64)

                    # Get model predictions
                    predicted = model.predict(val_inputs)

                    preds_lst = np.concatenate((preds_lst, np.array(predicted.cpu())))
                    true_labels_lst = np.concatenate((true_labels_lst, val_labels))

                # Calculate F1 score, recall and precision
                f1_score = sklearn.metrics.f1_score(true_labels_lst, preds_lst, average='binary') * 100
                recall = sklearn.metrics.recall_score(true_labels_lst, preds_lst, average='binary') * 100
                precision = sklearn.metrics.precision_score(true_labels_lst, preds_lst, average='binary') * 100

                # Early stopping if validation accuracy didn't increase in last 10 epochs
                if f1_score <= max_curr_f1:
                    cpt_early_stop += 1
                    if cpt_early_stop == 10:
                        return max_curr_f1
                elif f1_score > max_curr_f1:
                    max_curr_f1 = f1_score
                    cpt_early_stop = 0
                    if max_curr_f1 > get_best_f1_score(os.path.join(Path.cwd(), 'Classifier', 'models',
                                                               'best_' + self.model_name + '_params',
                                                               "best_f1_score_" + str(self.window_size) + ".txt")):
                        found_best_model = True
                        save_f1_score(f1_score, os.path.join(Path.cwd(), 'Classifier', 'models',
                                                               'best_' + self.model_name + '_params',
                                                               "best_f1_score_" + str(self.window_size) + ".txt"))
                        # Best model to save
                        torch.save(model,
                                   os.path.join(Path.cwd(), 'Classifier', 'models', 'best_' + self.model_name +
                                                '_params', 'model_' + str(self.window_size) + '.pth'))

                # store loss and iteration
                loss_list.append(loss.data)
                accuracy_list.append(f1_score)
                # Print Loss on train and accuracy on test each epoch
                print('Epoch: {}  Loss on train: {}  F1_score on validation set: {} %'.format(epoch + 1, loss.data.item(),
                                                                                              f1_score))
        # if best model founded, save loss history
        if found_best_model:
            save_loss_history(loss_list, accuracy_list,
                              os.path.join(base_path, 'best_' + self.model_name + '_params'),
                              self.window_size)
        return np.max(accuracy_list)

def save_loss_history(train_loss: list, validation_loss: list, directory, window_size):
    validation = np.concatenate((['validation_loss'], validation_loss))
    train = np.concatenate((['train_loss'], train_loss))
    dataset = pd.DataFrame(np.vstack((train, validation)))
    pd.DataFrame(dataset).to_csv(os.path.join(directory, 'val_train_loss_' + str(window_size) + '.csv'), header=False, index=False)


def nn_train(model_type, model_name, window_size_lst):
    for window_size in window_size_lst:
        trials = 10
        optuna.logging.set_verbosity(optuna.logging.WARNING)
        optuna.logging.disable_propagation()
        if model_type == 'LSTM' or model_type == 'RNN':
            # Original data prepared in windows and merged between driver and passenger (accelero + magneto)
            positive_data_path = Path(os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                                                   'window_merged', 'positive_usecases'))
            negative_data_path = Path(
                os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                             'window_merged', 'negative_usecases'))

            # Load data for passenger and driver
            train_data, validation_data, test_data = read_train_val_test_data(positive_data_path, negative_data_path)

            # Create optuna hyperparameter tuning instance
            sampler = optuna.samplers.TPESampler()
            study = optuna.create_study(direction="maximize", sampler=sampler, study_name=model_type + '_' + str(window_size) + '_study',
                                        storage='sqlite:///Classifier/models/best_' + model_name + '_params/' + model_type + '_' + str(window_size) + '_study.db',
                                        load_if_exists=True)

            study.optimize(ObjectiveRecurrentNetwork(train_data, validation_data, test_data, model_name, model_type, window_size), n_trials=trials, show_progress_bar=True)

        elif model_type == 'CNN':
            cnn_data_path = os.path.join(config.constants.get("CNN_ACCELERO_MAGNETO"), str(window_size))

            train = load_cnn_data(cnn_data_path, 'train')
            train_y = load_cnn_data(cnn_data_path, 'train_y')
            validation = load_cnn_data(cnn_data_path, 'validation')
            validation_y = load_cnn_data(cnn_data_path, 'validation_y')
            test = load_cnn_data(cnn_data_path, 'test')
            test_y = load_cnn_data(cnn_data_path, 'test_y')

            # Create optuna hyperparameter tuning instance
            sampler = optuna.samplers.TPESampler()
            study = optuna.create_study(direction="maximize", sampler=sampler, study_name='CNN_' + str(window_size) + '_study',
                                        storage='sqlite:///Classifier/models/best_cnn_params/CNN_' + str(window_size) + '_study.db',
                                        load_if_exists=True)
            # Optimize hyperparameters based on validation accuracy returned by the objective function
            study.optimize(ObjectiveCNN(train, train_y, validation, validation_y, test, test_y, window_size), n_trials=trials)

        # Get best accuracy
        print('Best accuracy on ', model_name, '(window of ', str(window_size), ')', ' was : ', study.best_value, ' with params : ', study.best_params)

        # Save optuna models history
        study_df = study.trials_dataframe()
        save_dataframe(os.path.join(Path.cwd(), 'Classifier', 'models', 'studies'), study_df,
                       model_name + '_' + str(window_size) + '_optuna_study')

    '''
    # Test best model
    print('########################################')
    print('Test best model on test data...')
    print('########################################')
    model = torch.load(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_' + model_name + '_params', 'model.pth'))
    model.eval()
    
    # Use best model with different window_size and perhaps differents data structure (permuted, etc)
    '''

if __name__ == '__main__':
    nn_train('RNN', 'rnn')
