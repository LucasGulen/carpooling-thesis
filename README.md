This repository contains all python code used in my master thesis, where the objective was to analyse carpooling data while try to classify pairs of people makin carpooling or not.

Once the repository is downloaded, the first thing to do is to go through data/link_to_download_data.txt and download the 20GB of data. The data directory downloaded has to replace the existing data directory.

To be able to execute the code, you will be first asked to installed Python >= 3.7.6.

Then you will have the choice to use Anaconda or pip to install the different packages required and given in the "requirements.txt" file.

The commands could be either "conda create --name <env_name> --file requirements.txt" or "pip install -r requirements.txt".

Then, the execution of the code will be done by executing the command : python main.py, inside the root directory of this project. The command will prompt to you some choices as which model you would like to use and for what kind of task, like training or testing.