import numpy as np

def stack_travels(data, dataset):
    for travel in data:
        driver = travel.driverData
        passenger = travel.passengersData[0]

        if dataset is None:
            dataset = np.vstack((driver, passenger))
        else:
            dataset = np.vstack((dataset, np.vstack((driver, passenger))))
    return dataset
