import sys

from Final_classifier.main import prompt_choices_to_user

sys.path.append('./')
import inquirer
from Classifier.main import prompt_choices_to_user as classifier_prompt
from Generative.main import prompt_choices_to_user as generative_prompt
import Final_classifier.train as train_final_classifier

task_types = [
    inquirer.List('task_type',
                  message="Which type of problem would you like to do deal with ?",
                  choices=['Only Classifier', 'Only generative', 'Final classification model'],
                  ),
]

if __name__ == '__main__':

    task = inquirer.prompt(task_types)["task_type"]
    if task == 'Only Classifier':
        classifier_prompt()
    elif task == 'Only generative':
        generative_prompt()
    elif task == 'Final classification model':
        prompt_choices_to_user()


