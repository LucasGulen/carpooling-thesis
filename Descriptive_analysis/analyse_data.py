from sklearn.utils.extmath import density

import config
from utils import read_data
from matplotlib import pyplot as plt
import seaborn as sns
from pandas.plotting import autocorrelation_plot
import numpy as np
import os
from pathlib import Path
from sklearn import preprocessing
from sklearn.decomposition import PCA

raw_positive = config.constants.get("RAW_POSITIVE_CASES")
raw_negative = config.constants.get("RAW_NEGATIVE_CASES")

def autocorr(x, t=25):
    return np.corrcoef(np.array([x[:-t], x[t:]]))

def get_stats(dataset):
    nb_frames = 0
    nb_travels = 0
    for travel in dataset:
        nb_frames += travel.driverData.shape[0]
        nb_travels += 1
    return nb_frames, nb_travels


if __name__ == '__main__':
    positive_dataset = read_data(raw_positive)
    negative_dataset = read_data(raw_negative)
    nb_frames_pos, nb_travels_pos = get_stats(positive_dataset)
    nb_frames_neg, nb_travels_neg = get_stats(negative_dataset)
    nb_frames_tot = nb_frames_neg + nb_frames_pos
    nb_travels_tot = nb_travels_neg + nb_travels_pos

    print('Total of positive frames = ', str(nb_frames_pos), str(nb_frames_pos / nb_frames_tot))
    print('Total of negative frames = ', str(nb_frames_neg), str(nb_frames_neg / nb_frames_tot))
    print('Total of frames = ', str(nb_frames_tot))

    print('Total of positive travels = ', str(nb_travels_pos), str(nb_travels_pos/nb_travels_tot))
    print('Total of negative travels = ', str(nb_travels_neg), str(nb_travels_neg/nb_travels_tot))
    print('Total of travels = ', str(nb_travels_tot))

    driver_example = positive_dataset[0].driverData
    passenger_example = positive_dataset[0].passengersData[0]

    ####################################
    # Plot autocorrelation for each sensor for the driver
    ####################################
    fig, axs = plt.subplots(3, figsize=(9, 9))
    autocorrelation_plot(driver_example['Accelerometer_x'][:500], label='x', ax=axs[0])
    autocorrelation_plot(driver_example['Accelerometer_y'][:500], label='y', ax=axs[0])
    autocorrelation_plot(driver_example['Accelerometer_z'][:500], label='z', ax=axs[0])
    axs[0].legend(loc='upper left', bbox_to_anchor=(0.1, 1.01), ncol=2,
                  borderaxespad=0, frameon=False)
    axs[0].set_title('Accelerometer')
    axs[0].set_ylim(-.5, 1)


    autocorrelation_plot(driver_example['Magnetometer_x'][:500], label='x', ax=axs[1])
    autocorrelation_plot(driver_example['Magnetometer_y'][:500], label='y', ax=axs[1])
    autocorrelation_plot(driver_example['Magnetometer_z'][:500], label='z', ax=axs[1])
    autocorrelation_plot(driver_example['Magnitude'][:500], label='magnitude', ax=axs[1])
    axs[1].legend(loc='upper left', bbox_to_anchor=(0.1, 1.01), ncol=2,
                  borderaxespad=0, frameon=False)
    axs[1].set_title('Magnetometer')
    axs[1].set_ylim(-.5, 1)

    autocorrelation_plot(driver_example['Gyroscope_x'][:500], label='x', ax=axs[2])
    autocorrelation_plot(driver_example['Gyroscope_y'][:500], label='y', ax=axs[2])
    autocorrelation_plot(driver_example['Gyroscope_z'][:500], label='z', ax=axs[2])
    axs[2].legend(loc='upper left', bbox_to_anchor=(0.1, 1.01), ncol=2,
                  borderaxespad=0, frameon=False)
    axs[2].set_title('Gyroscope')
    axs[2].set_ylim(-.5, 1)
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'autocorrelation_sensors.svg'), format='svg')
    ####################################

    ####################################
    # Plot each sensor with its magnitude (magnitude = sqrt(x^2, y^2, z^2)) base on a sample file
    ####################################
    driver_array = np.array(driver_example)
    driver_array = driver_array[:500, 0:9].astype(dtype=float)
    driver_array = preprocessing.normalize(driver_array)
    acc_magnitude = np.sqrt(np.sum(driver_array[:, 0:3]**2, axis=1))
    gyro_magnitude = np.sqrt(np.sum(driver_array[:, 3:6]**2, axis=1))
    magneto_magnitude = np.sqrt(np.sum(driver_array[:, 6:9]**2, axis=1))

    fig, axs = plt.subplots(3, figsize=(9, 9))
    axs[0].plot(driver_array[:, 0], marker='.', linestyle='-', linewidth=0.5, color='green', label='x')
    axs[0].plot(driver_array[:, 1], marker='.', linestyle='-', linewidth=0.5, color='red', label='y')
    axs[0].plot(driver_array[:, 2], marker='.', linestyle='-', linewidth=0.5, color='orange', label='z')
    axs[0].plot(acc_magnitude, marker='.', linestyle='-', linewidth=0.5, color='black', label='magnitude')
    axs[0].legend(loc='lower left', bbox_to_anchor=(0.1, 1.01), ncol=2,
                  borderaxespad=0, frameon=False)
    axs[0].set_title('Accelerometer')
    axs[0].set_ylabel('Velocity in $m/s^2$')

    axs[1].plot(driver_array[:, 3], marker='.', linestyle='-', linewidth=0.5, color='green', label='x')
    axs[1].plot(driver_array[:, 4], marker='.', linestyle='-', linewidth=0.5, color='red', label='y')
    axs[1].plot(driver_array[:, 5], marker='.', linestyle='-', linewidth=0.5, color='orange', label='z')
    axs[1].plot(gyro_magnitude, marker='.', linestyle='-', linewidth=0.5, color='black', label='magnitude')
    axs[1].set_title('Gyroscope')
    # axs[1].legend(loc='lower left', bbox_to_anchor=(0.3, 1.01), ncol=2,
              # borderaxespad=0, frameon=False)
    axs[1].set_ylabel('Angular rotational velocity in $rad/s$')

    axs[2].plot(driver_array[:, 6], marker='.', linestyle='-', linewidth=0.2, color='green', label='x')
    axs[2].plot(driver_array[:, 7], marker='.', linestyle='-', linewidth=0.2, color='red', label='y')
    axs[2].plot(driver_array[:, 8], marker='.', linestyle='-', linewidth=0.2, color='orange', label='z')
    axs[2].plot(magneto_magnitude, marker='.', linestyle='-', linewidth=0.2, color='black', label='magnitude')
    # axs[2].legend(loc='lower left', bbox_to_anchor=(0.3, 1.01), ncol=2,
              # borderaxespad=0, frameon=False)
    axs[2].set_title('Magnetometer')
    axs[2].set_ylabel('Magnetic field in $\mu T$')
    plt.xlabel('Time expressed in number of frames (1 frame = 500ms)')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'sensors_sample_normalized.svg'), format='svg')
    ####################################

    ####################################
    # Plot density function on each sensor
    ####################################
    driver_array = np.array(driver_example)[:, 0:9]
    driver_array = driver_array.astype(dtype=float)
    '''
    fig = plt.figure()
    sns.set_style('whitegrid')
    sns.kdeplot(np.array(driver_array[:, 0]), bw=0.5)
    sns.kdeplot(np.array(driver_array[:, 1]), bw=0.5)
    sns.kdeplot(np.array(driver_array[:, 2]), bw=0.5)
    plt.show()
    '''


    # Create PCA model
    pca = PCA(n_components=1, svd_solver='randomized')

    # Standardize data before PCA
    standardized_driver = (driver_array - driver_array.mean(axis=0)) / driver_array.std(axis=0)

    # Use PCA to have only 1 component by sensor
    pca_accelero = pca.fit_transform(standardized_driver[:, 0:3]).reshape(-1)
    pca_gyro = pca.fit_transform(standardized_driver[:, 3:6]).reshape(-1)
    pca_magneto = pca.fit_transform(standardized_driver[:, 6:9]).reshape(-1)

    fig, axs = plt.subplots(3)
    sns.set_style('whitegrid')
    sns.kdeplot(np.array(pca_accelero), bw=0.5, ax=axs[0], shade=True)
    axs[0].set_title('Accelerometer')
    axs[0].set_ylabel('$P(x)$')
    axs[0].set_xlabel('Velocity in $m/s^2$')

    sns.set_style('whitegrid')
    sns.kdeplot(np.array(pca_magneto), bw=0.5, ax=axs[1], shade=True, color='orange')
    axs[1].set_title('Magnetometer')
    axs[1].set_ylabel('$P(x)$')
    axs[1].set_xlabel('Magnetic field in $\mu T$')

    sns.set_style('whitegrid')
    sns.kdeplot(np.array(pca_gyro), bw=0.5, ax=axs[2], shade=True, color='green')
    axs[2].set_title('Gyroscope')
    axs[2].set_ylabel('$P(x)$')
    axs[2].set_xlabel('Angular rotational velocity in $rad/s$')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'sensors_density_distribution.svg'), format='svg')

    print('std gyro : ', str(np.std(pca_gyro)))
    print('std magneto : ', str(np.std(pca_magneto)))
    print('std accelero : ', str(np.std(pca_accelero)))

    print('mean gyro : ', str(np.mean(pca_gyro)))
    print('mean magneto : ', str(np.mean(pca_magneto)))
    print('mean accelero : ', str(np.mean(pca_accelero)))


    pass


    '''
    # Plots statistics above
    plt.figure(figsize=(3.5, 3))
    plot = sns.barplot(x=['Positive', 'Negative'], y=[nb_travels_neg/nb_travels_tot*100, nb_travels_pos/nb_travels_tot*100])
    plt.ylim(0, 100)
    plt.xlabel('Label classes')
    plt.ylabel('Distribution in percents')
    plt.title('Travel distribution by label')
    plt.tight_layout()
    plt.show()
    # plt.savefig('bar_plot_travel_distribution.svg')
    '''

