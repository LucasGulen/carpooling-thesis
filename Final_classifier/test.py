import os
from pathlib import Path
import sklearn
from scipy.fftpack import fft
from sklearn.metrics import mean_squared_error
import torch
import numpy as np
import Generative.config as config
import matplotlib.pyplot as plt
import warnings
from Classifier.utils import read_train_val_test_data
from sklearn.decomposition import PCA
import seaborn as sns

warnings.filterwarnings("ignore")
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
base_path = os.path.join(Path.cwd(), 'Final_classifier', 'models')


def standardize(mean, std, data):
    return (data - mean) / std


def classifier_test(test, y_test, window_size, model_name: str, model_file: str):

    # Standardize data
    mean = torch.FloatTensor(np.load(os.path.join(base_path, 'best_' + model_name + '_classifier', 'mean_train_' + str(window_size) + '.npy'))).to(DEVICE)
    std = torch.FloatTensor(np.load(os.path.join(base_path, 'best_' + model_name + '_classifier', 'std_train_' + str(window_size) + '.npy'))).to(DEVICE)
    test_standardized = standardize(mean, std, test)

    # Load model in eval mode
    model = torch.load(
        os.path.join(base_path, 'best_' + model_name + '_classifier', model_file))
    model.eval()
    for parameter in model.encoder.parameters():
        parameter.requires_grad = False

    inputs = test_standardized.view(-1, window_size * 7 * 2, 1)

    outputs, z = model.predict(inputs)
    f1_score = sklearn.metrics.f1_score(y_test, outputs.data.cpu(), average='binary') * 100

    print('F1 score for final classifier on test data : ', f1_score)

    return f1_score, z, mean, std


def pca_reduction(encoded, raw, mean, std, window_size, nb_components, model_name: str):
    # This show density probability function on data
    # Create PCA model
    pca = PCA(n_components=nb_components, svd_solver='randomized')

    # Standardize data before PCA
    raw = (raw.cpu() - mean.cpu()) / std.cpu()

    print(encoded)

    # Use PCA to have only 1 component by sensor
    pca_raw = pca.fit_transform(raw).reshape(-1)
    pca_encoded = pca.fit_transform(encoded).reshape(-1)

    # Create and save a probability density plot with each data distributions
    fig = plt.figure()
    sns.set_style('whitegrid')
    sns.kdeplot(np.array(pca_raw), bw=0.5, shade=True)
    sns.kdeplot(np.array(pca_encoded), bw=0.5, shade=True)
    plt.legend(np.array(['x', 'z-' + model_name.upper()]), ncol=2, loc='upper right')
    plt.ylabel('$P(x)$')
    plt.xlabel('Component values')
    # plt.title('Probability density plot of 1-components distributions (w_size=' + str(window_size) + ')')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'results', 'pdfplot' + model_name.upper() + '_' + str(window_size) + '.svg'), format='svg')


    # This show scatter plots
    # Create PCA model
    pca = PCA(n_components=2, svd_solver='randomized')

    # Use PCA to have only 1 component by sensor
    pca_raw = pca.fit_transform(raw)
    pca_encoded = pca.fit_transform(encoded)

    # Create and save a scatter plot with each data distributions
    fig = plt.figure()
    sns.set_style('whitegrid')
    sns.scatterplot(np.array(pca_raw[:, 0]), np.array(pca_raw[:, 1]), marker='+', alpha=0.5)
    sns.scatterplot(np.array(pca_encoded[:, 0]), np.array(pca_encoded[:, 1]), marker='+', alpha=0.5)
    plt.legend(np.array(['x', 'z-' + model_name.upper()]), loc='upper right')
    # plt.title('Scatter plot of 2-components distributions (w_size=' + str(window_size) + ')')
    plt.ylabel('Component 1')
    plt.xlabel('Component 2')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'results', 'scatterplot_' + model_name.upper() + '_' + str(window_size) + '.svg'), format='svg')



def test () :
    # Load data for MLP and CNN Auto Encoder
    data_path_positive = Path(
        os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(5),
                     'window_merged', 'positive_usecases'))
    data_path_negative = Path(
        os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(5),
                     'window_merged', 'negative_usecases'))

    _, _, test_data = read_train_val_test_data(data_path_positive, data_path_negative)
    y_test = test_data[:, test_data.shape[1] - 1]
    test = np.delete(test_data, test_data.shape[1] - 1, 1)
    test = torch.Tensor(test).to(DEVICE)

    # Test model with auto encoder pre trained
    f1_score, z, mean, std = classifier_test(test, y_test, 5, 'AE', 'model.pth')
    pca_reduction(z.squeeze(-1).cpu(), test.cpu(), mean.cpu(), std.cpu(), 5, 2, 'Frozen-AE-LSTM')

    # Test model with auto encoder trained again
    f1_score, z, mean, std = classifier_test(test, y_test, 5, 'AE', 'model_trainable.pth')
    pca_reduction(z.squeeze(-1).cpu(), test.cpu(), mean.cpu(), std.cpu(), 5, 2, 'Trainable-AE-LSTM')
