import os
import Generative.config as config
from Generative.data_prep_AE.utils import prepare_encoder_data
from Generative.models.auto_encoder.mlp import MLP
from Generative.models.auto_encoder.cnn import CNN
from Generative.models.auto_encoder.cnn_rae import CNN_RAE
from Generative.models.auto_encoder.lstm import LSTMAE

from Final_classifier.models.AE_classifier import AEClassifier
from Generative.utils import get_best_mse, save_mse
from Classifier.utils import prepare_data_loader, read_train_val_test_data, save_f1_score, get_best_f1_score
from Generative.data_preparation.utils import read_data, save_dataframe
from torch import nn
import torch
import optuna
from optuna import Trial
import torch.optim as optim
import numpy as np
import sklearn
from pathlib import Path
import pandas as pd

N_EPOCHS = 50
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
base_path = os.path.join(Path.cwd(), 'Generative', 'models', 'auto_encoder')

def find_best_metric_score(scores_model_dir, classification):
    if classification:
        best_metric = 0
    else:
        best_metric = 100000000
    for dir in os.listdir(scores_model_dir):
        metric_dir = os.path.join(scores_model_dir, dir)
        if not os.path.isdir(metric_dir) and 'best' in dir:
            # read f1_score and store it if it is better
            model_score = get_best_f1_score(metric_dir)
            if (model_score > best_metric and classification) or (model_score < best_metric and not classification):
                best_metric = model_score
            pass
    return best_metric


def get_best_model_name(models_base_dir, classification):
    if classification:
        best_metric = 0
        base_model_results_path = os.path.join(Path.cwd(), models_base_dir, 'models')
    else:
        best_metric = 100000000
        base_model_results_path = os.path.join(Path.cwd(), models_base_dir, 'models', 'auto_encoder')
    # Navigate through all params model directory
    for dir in os.listdir(base_model_results_path):
        # Check if it is directory and if it contains model parameters
        if os.path.isdir(os.path.join(base_model_results_path, dir)) and 'params' in dir:
                model_dir = os.path.join(base_model_results_path, dir)
                current_model_metric = find_best_metric_score(model_dir, classification)
                # store model name and f1 if current f1 is higher
                if (current_model_metric > best_metric and classification) or (current_model_metric < best_metric and not classification):
                    best_metric = current_model_metric
                    best_model_path = dir
    model_name = best_model_path.split(sep='_')[1].lower()
    model_type = model_name.upper()
    return model_type, model_name


class Objective(object):
    def __init__(self, train_data, validation_data, test_data, classifier_type, classifier_name, window_size, train_encoder):
        self.train_data = train_data
        self.validation_data = validation_data
        self.test_data = test_data
        self.classifier_type = classifier_type
        self.classifier_name = classifier_name
        self.window_size = window_size
        self.train_encoder = train_encoder

    def __call__(self, trial: Trial):
        batch_size = trial.suggest_int('batch_size', 128, 512, step=32)
        trial.suggest_int('num_layers', 1, 3, step=1)
        learning_rate = trial.suggest_loguniform('lr', 1e-4, 1e-1)
        trial.suggest_uniform("dropout", 1e-4, 3e-1)
        trial.suggest_int('hidden_size', 32, 256)

        # Get best classifier name
        classifier_type, classifier_name = get_best_model_name('Classifier', True)
        # Get best classifier optuna study
        model_study = optuna.load_study(study_name=self.classifier_type + '_' + str(self.window_size) + '_study',
                                        storage='sqlite:///Classifier/models/best_' + self.classifier_name + '_params/' +
                                                self.classifier_type + '_' + str(self.window_size) + '_study.db')
        '''
        # Get best classifier parameters
        batch_size = model_study.best_params['batch_size']
        learning_rate = model_study.best_params['lr']
        '''
        window_size = self.window_size

        # Get best encoder model
        # encoder_type, encoder_name = get_best_model_name('Generative', False)
        encoder_type, encoder_name = 'LSTMAE', 'lstmae'
        PATH = os.path.join(Path.cwd(), 'Generative', 'models', 'auto_encoder', 'best_lstmae_params', 'model_' + str(self.window_size) + '.pth')
        pre_trained_ae = torch.load(PATH)

        # Get best encoder optuna study
        encoder_study = optuna.load_study(study_name='lstmae_' + str(window_size) + '_study',
                                          storage='sqlite:///Generative/models/auto_encoder/best_lstmae_params/lstmae_' + str(
                                            window_size) + '_study.db')

        # train_loader, validation_loader, test_loader = prepare_encoder_data(batch_size, window_size, '', train_data, validation_data, test_data)

        train_loader, validation_loader, test_loader = prepare_data_loader(batch_size, self.train_data,
                                                                           self.validation_data,
                                                                           self.test_data)
        torch.cuda.empty_cache()

        # model_params = {key: value for key, value in model_study.best_params.items() if key != 'batch_size' and key != 'lr'}
        # hidden = model_study.best_params.get('hidden_size')
        # trial.suggest_int('hidden_size', hidden, hidden)

        model_params = {key: value for key, value in trial.params.items() if key != 'batch_size' and key != 'lr'}
        model = AEClassifier(encoder_type, classifier_type, pre_trained_ae, window_size * 7 * 2, model_params, train_encoder = self.train_encoder)
        model.to(DEVICE)

        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)

        found_best_model = False
        max_curr_f1 = 0
        cpt_early_stop = 0
        train_loss_list = []
        val_f1_list =[]
        print('################################')
        print('AE LSTM training with params (window_size=', window_size, ') : ', trial.params)
        print('################################')
        for epoch in range(N_EPOCHS):  # loop over the dataset multiple times
            model.classifier.train()
            if self.train_encoder:
                model.encoder.train()
            training_loss = 0
            # TRAINING ROUND
            for train_inputs, labels in train_loader:
                # zero the parameter gradients
                optimizer.zero_grad()

                inputs = train_inputs.float()

                inputs, labels = inputs.to(DEVICE), labels.to(device=DEVICE, dtype=torch.float)

                if 'LSTM' in classifier_type or 'RNN' in classifier_type:
                    inputs = inputs.view(-1, window_size * 7 * 2, 1)

                # forward + backward + optimize
                outputs, z = model(inputs)

                # print('outputs : ', outputs.size())

                loss = criterion(outputs.view(-1), labels)
                training_loss += loss
                loss.cuda()
                loss.backward()
                with torch.no_grad():
                    optimizer.step()

            training_loss = training_loss / len(train_loader)
            # Calculate accuracy on validation set after each epoch
            with torch.set_grad_enabled(False):
                true_labels_lst = []
                preds_lst = []
                # Iterate through test dataset
                for val_inputs, val_labels in validation_loader:
                    val_inputs = val_inputs.float()
                    val_inputs, labels = val_inputs.cuda(), val_labels.to(device='cuda:0', dtype=torch.float)

                    if 'LSTM' in classifier_type or 'RNN' in classifier_type:
                        val_inputs = val_inputs.view(-1, window_size * 7 * 2, 1)

                    # Forward propagation
                    preds, z = model.predict(val_inputs)

                    # Get predictions from the maximum value
                    preds_lst = np.concatenate((preds_lst, np.array(preds.cpu())))
                    true_labels_lst = np.concatenate((true_labels_lst, val_labels))

                # Calculate F1 score, recall and precision
                f1_score = sklearn.metrics.f1_score(true_labels_lst, preds_lst, average='binary') * 100

                # Early stopping if validation accuracy didn't increase in last 10 epochs
                if f1_score <= max_curr_f1:
                    cpt_early_stop += 1
                    if cpt_early_stop == 10:
                        return max_curr_f1
                elif f1_score > max_curr_f1:
                    max_curr_f1 = f1_score
                    cpt_early_stop = 0
                    if max_curr_f1 > get_best_f1_score(os.path.join(Path.cwd(), 'Final_classifier', 'models',
                                                                    'best_AE_classifier',
                                                                    "best_f1_score.txt")):
                        if self.train_encoder:
                            save_f1_score(f1_score, os.path.join(Path.cwd(), 'Final_classifier', 'models',
                                                                 'best_AE_classifier',
                                                                 "best_f1_score_trainable.txt"))
                            # Best model to save
                            torch.save(model,
                                       os.path.join(Path.cwd(), 'Final_classifier', 'models', 'best_AE_classifier',
                                                    'model_trainable.pth'))
                        else:
                            save_f1_score(f1_score, os.path.join(Path.cwd(), 'Final_classifier', 'models',
                                                                 'best_AE_classifier',
                                                                 "best_f1_score.txt"))
                            # Best model to save
                            torch.save(model,
                                       os.path.join(Path.cwd(), 'Final_classifier', 'models', 'best_AE_classifier',
                                                    'model.pth'))


                # store loss and iteration
                train_loss_list.append(training_loss)
                val_f1_list.append(f1_score)
                # Print Loss on train and accuracy on test each epoch
                print('Epoch: {}  Loss on train: {}  F1_score on validation set: {} %'.format(epoch + 1, training_loss,
                                                                                        f1_score))
        return np.max(val_f1_list)


def save_loss_history(train_loss: list, validation_loss: list, directory, window_size):
    validation = np.concatenate((['validation_loss'], validation_loss))
    train = np.concatenate((['train_loss'], train_loss))
    dataset = pd.DataFrame(np.vstack((train, validation)))
    pd.DataFrame(dataset).to_csv(os.path.join(directory, 'val_train_loss_' + str(window_size) + '.csv'), header=False,
                                 index=False)


def train_model():
    train_encoder = True
    # Load data
    data_path_positive = Path(
        os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(5),
                     'window_merged', 'positive_usecases'))
    data_path_negative = Path(
        os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(5),
                     'window_merged', 'negative_usecases'))

    train_data, validation_data, test_data = read_train_val_test_data(data_path_positive, data_path_negative)

    mean = np.load(os.path.join(base_path, 'best_lstmae_params', 'mean_train_5.npy'))
    std = np.load(os.path.join(base_path, 'best_lstmae_params', 'std_train_5.npy'))

    path = os.path.join(Path.cwd(), 'Final_classifier', 'models', 'best_AE_classifier')
    np.save(os.path.join(path, 'mean_train_5'), mean)
    np.save(os.path.join(path, 'std_train_5'), std)

    # Standardization
    train_data[:, 0:train_data.shape[1] - 1] = (train_data[:, 0:train_data.shape[1] - 1] - mean) / std
    validation_data[:, 0:validation_data.shape[1] - 1] = (validation_data[:, 0:validation_data.shape[1] - 1] - mean) / std

    # Create optuna hyperparameter tuning instance
    sampler = optuna.samplers.TPESampler()

    if train_encoder:
        study = optuna.create_study(direction="minimize", sampler=sampler, study_name='lstmae_lstmc_trainable_5_study',
                                    storage='sqlite:///Final_classifier/models/best_AE_classifier/lstmae_lstmc_trainable_5_study.db',
                                    load_if_exists=True)
    else:
        study = optuna.create_study(direction="minimize", sampler=sampler, study_name='lstmae_lstmc_5_study',
                                    storage='sqlite:///Final_classifier/models/best_AE_classifier/lstmae_lstmc_5_study.db',
                                    load_if_exists=True)

    study.optimize(Objective(train_data, validation_data, test_data, 'LSTM', 'lstm', 5, train_encoder), n_trials=10, show_progress_bar=True)

    # Get best accuracy
    print('Best f1 score on was : ', study.best_value,
          ' with params : ', study.best_params)

    # Save optuna models history
    study_df = study.trials_dataframe()
    save_dataframe(os.path.join(Path.cwd(), 'Final_classifier', 'models', 'studies'), study_df,
                   'lstmae_lstmc_5_optuna_study')

# window_size_lst = config.constants.get("WINDOW_SIZE_LST")
# nn_train('AutoEncoder', 'mlp', window_size_lst)

if __name__ == '__main__':
    train_model()
