import sys

from Final_classifier.test import test

sys.path.append('./')
import inquirer
from Classifier.main import prompt_choices_to_user as classifier_prompt
from Generative.main import prompt_choices_to_user as generative_prompt
import Final_classifier.train as train_final_classifier

task_types = [
    inquirer.List('task_type',
                  message="Would you like to train or test the final stacked model ?",
                  choices=['Train', 'Test'],
                  ),
]

def prompt_choices_to_user():
    task = inquirer.prompt(task_types)["task_type"]
    if task == 'Train':
        print('Load pre-trained auto encoder and train only the LSTM classifier stacked ...')
        train_final_classifier.train_model()
    elif task == 'Test':
        print('Load and test pre-trained auto encoder with LSTM classifier stacked ...')
        test()


if __name__ == '__main__':
    prompt_choices_to_user()



