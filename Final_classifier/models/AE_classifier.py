import torch
from torch import nn

from Generative.models.auto_encoder.mlp import MLP
from Generative.models.auto_encoder.lstm import LSTMAE
from Classifier.models.lstm import LSTM


class AEClassifier(nn.Module):
    def __init__(self, encoder_class_name, classifier_class_name, pre_trained_ae, input_shape, classifier_params, train_encoder=False):
        super(AEClassifier, self).__init__()

        # z_shape = input_shape//8
        self.classifier_seq_dim = input_shape
        self.classifier_inputs_dim = 1
        self.classifier_output_dim = 1

        # Load encoder
        self.encoder = pre_trained_ae
        '''
        encoder_class = globals()[encoder_class_name]
        if 'LSTM' in encoder_class_name or 'RNN' in encoder_class_name:
            self.encoder = encoder_class(1, input_shape, encoder_params)
        else:
            self.encoder = encoder_class(input_shape)
        '''
        # Load pre trained weights for encoder and freeze gradient update
        # self.init_encoder_weights(encoder_w)

        if not train_encoder:
            self.encoder.eval()
            for parameter in self.encoder.parameters():
                parameter.requires_grad = False

        # Load classifier
        classifier_class = globals()[classifier_class_name]
        self.classifier = classifier_class(self.classifier_inputs_dim,
                               self.classifier_output_dim, classifier_params)

    def init_encoder_weights(self, w):
        self.encoder.load_state_dict(w)
        pass

    def forward(self, x):
        # print('x : ', x.size())

        z = self.encoder.forward(x, only_encoder=True)

        # print('z : ', z.size())

        # z = z.view(-1, self.classifier_seq_dim, self.classifier_inputs_dim).float()

        z = z.unsqueeze(-1)
        # print('z view : ', z.size())

        out = self.classifier(z)
        return out, z


    def predict(self, x):
        outputs, z = self.forward(x)
        outputs = torch.sigmoid(outputs.data)
        return torch.round(outputs).view(-1), z

