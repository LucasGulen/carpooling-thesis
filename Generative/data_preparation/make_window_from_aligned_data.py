import pandas as pd
import Generative.config as config
from Generative.data_preparation.utils import read_data, save_data, save_numpy
import numpy as np
import os
from multiprocessing import Pool
import itertools
import time


# CONSTANTS
data_path = config.constants.get('DATA_PATH')

# Data augmented paths with axis permutations (Accelerometer and magnetometer)
data_positive_cases_path = config.constants.get("ALIGNED_AUGMENTED_20_POSITIVE_CASES_PATH")
data_negative_cases_path = config.constants.get("ALIGNED_AUGMENTED_20_NEGATIVE_CASES_PATH")

# Original data paths (Accelerometer and magnetometer)
original_data_positive_cases_path = config.constants.get("ALIGNED_POSITIVE_CASES_PATH")
original_data_negative_cases_path = config.constants.get("ALIGNED_NEGATIVE_CASES_PATH")
# CONSTANTS


def window_to_row(data_driver, data_passenger, only_accelero):
    # Delete magnetometer columns (x, y, z magnitude) if only_accelerometer is set to true
    if only_accelero:
        data_driver = np.array(data_driver)
        data_passenger = np.array(data_passenger)
        data_driver = np.delete(data_driver, (3, 4, 5, 6), 1)
        data_passenger = np.delete(data_passenger, (3, 4, 5, 6), 1)
    else:
        data_driver = np.array(data_driver)
        data_passenger = np.array(data_passenger)

    # Extract y from last frame for this window
    y_driver = data_driver[data_driver.shape[0] - 1, data_driver.shape[1] - 1]
    y_passenger = data_passenger[data_passenger.shape[0] - 1, data_passenger.shape[1] - 1]

    # Delete each frame y label
    data_driver = np.delete(data_driver, data_driver.shape[1] - 1, 1)  # remove last column
    data_passenger = np.delete(data_passenger, data_passenger.shape[1] - 1, 1)  # remove last column

    # Transform array of n frame into one row of n column (window)
    data_driver, data_passenger = data_driver.flatten(order='F'), data_passenger.flatten(order='F')

    # Return the window with the label at the end
    data_driver, data_passenger = np.hstack((data_driver, y_driver)), np.hstack((data_passenger, y_passenger))

    return data_driver, data_passenger

def __parallel_prepare_windowed_data(travel, window_size, only_accelero):
    i = 0
    lst_driver = []
    lst_passenger = []
    # Loop through the current travel driver data. Remind that driver and passenger data are already aligned here (same size)
    while i < travel.driverData.shape[0] - (window_size - 1):
        window_rowed_driver, window_rowed_passenger = window_to_row(
            travel.driverData[i: i + window_size],
            travel.passengersData[0][i: i + window_size], only_accelero)
        lst_driver.append(window_rowed_driver)
        lst_passenger.append(window_rowed_passenger)
        i += 1

    # From list of windows create a dataframe for passenger and one for driver
    df_driver = pd.DataFrame(lst_driver)
    df_passenger = pd.DataFrame(lst_passenger)

    # Store the dataframe in the current travel
    travel.driverWindows = df_driver
    travel.passengerWindows = df_passenger

    return travel

def prepare_windowed_data(dataset, window_size, only_accelero):
    # Multi-process saving variables
    agents = 20
    chunksize = 1
    # Parralelisation need list to iterate over each thread launched
    window_sizes = itertools.repeat(window_size, len(dataset))
    lst_only_accelero = itertools.repeat(only_accelero, len(dataset))

    dataset_zipped = zip(dataset, window_sizes, lst_only_accelero)

    # Call function __parallel_write_window_separate with multiple process over dataset
    with Pool(processes=agents) as pool:
        travels = pool.starmap(__parallel_prepare_windowed_data, dataset_zipped, chunksize)
    return travels

def merge_d_p_window(dataset):
    for travel in dataset:
        driver, passenger = np.array(travel.driverWindows), np.array(travel.passengerWindows)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # Delete y from driver and keep the passenger y label (it is the same for the training model phase, keep in mind and change this in testing phase)
        y = passenger[:, passenger.shape[1] - 1]
        driver = np.delete(driver, driver.shape[1] - 1, 1)
        passenger = np.delete(passenger, passenger.shape[1] - 1, 1)
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        # Concatenate n passenger windows matrix with n driver windows matrix into one matrix of n columns with n windows
        windows = np.stack((driver, passenger), axis=-1)
        windows = np.reshape(windows, (windows.shape[0], windows.shape[1] * windows.shape[2]))
        # Add y label to the end of the dataset
        windows = np.append(windows, y[:, np.newaxis], axis=1)

        travel.window_d_p_merged = windows


def cnn_prep(window_size, dataset):
    train, validation, test = None, None, None
    train_y, validation_y, test_y = None, None, None
    # prep windows data only for cnn (special preparation)
    for travel in dataset:
        driver = np.array(travel.driverData)
        passenger = np.array(travel.passengersData[0])
        windows = []

        '''
        # Minmax normalization
        # Indide de l'avant dernière colone. Utiliser pour éviter de normaliser le label
        idx_last = driver.shape[1] - 1

        min_driver = driver[:, 0:idx_last].min(axis=0)
        max_driver = driver[:, 0:idx_last].max(axis=0)

        min_passenger = passenger[:, 0:idx_last].min(axis=0)
        max_passenger = passenger[:, 0:idx_last].max(axis=0)

        driver[:, 0:idx_last] = (driver[:, 0:idx_last] - min_driver) / (max_driver - min_driver)
        passenger[:, 0:idx_last] = (passenger[:, 0:idx_last] - min_passenger) / (max_passenger - min_passenger)
        '''

        for i in range(0, len(driver) - window_size):
            acc_x_d, acc_x_p = driver[i:i + window_size, 0], passenger[i:i + window_size, 0]
            magn_x_d, magn_x_p = driver[i:i + window_size, 3], passenger[i:i + window_size, 3]

            acc_y_d, acc_y_p = driver[i:i + window_size, 1], passenger[i:i + window_size, 1]
            magn_y_d, magn_y_p = driver[i:i + window_size, 4], passenger[i:i + window_size, 4]

            acc_z_d, acc_z_p = driver[i:i + window_size, 2], passenger[i:i + window_size, 2]
            magn_z_d, magn_z_p = driver[i:i + window_size, 5], passenger[i:i + window_size, 5]

            magni_d, magni_p = driver[i:i + window_size, 6], passenger[i:i + window_size, 6]

            x = np.vstack((np.concatenate((acc_x_d, magn_x_d)), np.concatenate((acc_x_p, magn_x_p))))
            y = np.vstack((np.concatenate((acc_y_d, magn_y_d)), np.concatenate((acc_y_p, magn_y_p))))
            z = np.vstack((np.concatenate((acc_z_d, magn_z_d)), np.concatenate((acc_z_p, magn_z_p))))

            if len(windows) > 0:
                windows[0].append(x)
                windows[1].append(y)
                windows[2].append(z)
                tmp_label.append(driver[i, -1])
            else:
                windows.append([x])
                windows.append([y])
                windows.append([z])
                tmp_label = [driver[i, -1]]

        tmp_dataset = np.array(windows)
        tmp_dataset = np.reshape(tmp_dataset, (
        tmp_dataset.shape[1], tmp_dataset.shape[0], tmp_dataset.shape[2], tmp_dataset.shape[3]))
        tmp_label = np.array(tmp_label)
        train_ratio = config.constants.get('SPLIT_RATIO')
        split_idx = int(train_ratio * tmp_dataset.shape[0])
        if train is None:
            train = tmp_dataset[0:split_idx, :, :, :]
            train_y = tmp_label[0:split_idx]

            test = tmp_dataset[split_idx:tmp_dataset.shape[0], :, :, :]
            test_y = tmp_label[split_idx:tmp_label.shape[0]]

            validation = test[test.shape[0] // 2: test.shape[0]]
            validation_y = test_y[test_y.shape[0] // 2: test_y.shape[0]]

            test = test[0:test.shape[0] // 2]
            test_y = test_y[0:test_y.shape[0] // 2]
        else:
            train = np.vstack((train, tmp_dataset[0:split_idx, :, :, :]))
            train_y = np.hstack((train_y, tmp_label[0:split_idx]))

            tmp_test = tmp_dataset[split_idx:tmp_dataset.shape[0], :, :, :]
            tmp_test_y = tmp_label[split_idx:tmp_label.shape[0]]

            validation = np.vstack((validation, tmp_test[tmp_test.shape[0] // 2: tmp_test.shape[0]]))
            validation_y = np.hstack((validation_y, tmp_test_y[tmp_test_y.shape[0] // 2: tmp_test_y.shape[0]]))

            test = np.vstack((test, tmp_test[0:tmp_test.shape[0] // 2]))
            test_y = np.hstack((test_y, tmp_test_y[0:tmp_test_y.shape[0] // 2]))

    return train, train_y, validation, validation_y, test, test_y


if __name__ == '__main__':
    only_cnn_prep = True
    # Prepare data with only Accelerometer and then with accelerometer and gyroscope
    only_accelero = [True, False]
    for val in only_accelero:
        # Read aligned dataset files for both positive and negative cases
        positive_dataset = read_data(original_data_positive_cases_path)
        negative_dataset = read_data(original_data_negative_cases_path)

        if not only_cnn_prep:
            for window_size in config.constants.get("WINDOW_SIZE_LST"):
                    start_time = time.time()

                    # Create window of "window_size" and transform each window into a row (c.f xls file) and keep seperate passenger and driver
                    print('Preparing window positive dataset for window size of ', window_size, '...')
                    positive_dataset = prepare_windowed_data(positive_dataset, window_size, val)
                    print(time.time() - start_time)
                    start_time = time.time()
                    print('Preparing window negative dataset for window size of ', window_size, '...')
                    negative_dataset = prepare_windowed_data(negative_dataset, window_size, val)
                    print(time.time() - start_time)

                    print('Saving window data (no merge between passenger and driver)...')
                    # Save it in "data/windows/window_size/window_separate"
                    if val:
                        # save_data(os.path.join(data_path, 'windows_augmented_20_accelero', str(window_size)), positive_dataset, negative_dataset, 'window_separate', 'window')
                        save_data(os.path.join(data_path, 'windows_accelero', str(window_size)), positive_dataset, negative_dataset, 'window_separate', 'window')
                    else:
                        # save_data(os.path.join(data_path, 'windows_augmented_20', str(window_size)), positive_dataset, negative_dataset, 'window_separate', 'window')
                        save_data(os.path.join(data_path, 'windows', str(window_size)), positive_dataset, negative_dataset, 'window_separate', 'window')
                    print('window data saved (no merge between passenger and driver)')

                    # Now merge passenger and driver window
                    print('Merging window of driver and passenger into one dataset...')
                    merge_d_p_window(positive_dataset)
                    merge_d_p_window(negative_dataset)
                    print('Merge terminated successfully')

                    print('Saving window data (merge between passenger and driver)...')
                    # Save it in "data/windows/window_size/window_merged"
                    if val:
                        # save_data(os.path.join(data_path, 'windows_augmented_20_accelero', str(window_size)), positive_dataset, negative_dataset, 'window_merged', 'window_d_p_merged')
                        save_data(os.path.join(data_path, 'windows_accelero', str(window_size)), positive_dataset, negative_dataset, 'window_merged', 'window_d_p_merged')
                    else:
                        # save_data(os.path.join(data_path, 'windows_augmented_20', str(window_size)), positive_dataset, negative_dataset, 'window_merged', 'window_d_p_merged')
                        save_data(os.path.join(data_path, 'windows', str(window_size)), positive_dataset, negative_dataset, 'window_merged', 'window_d_p_merged')
                    print('window data saved (merge between passenger and driver)')

        else:
            print('Begin CNN data preparation ...')
            for window_size in config.constants.get("WINDOW_SIZE_LST"):
                train_p, train_y_p, validation_p, validation_y_p, test_p, test_y_p = cnn_prep(window_size, positive_dataset)
                train_n, train_y_n, validation_n, validation_y_n, test_n, test_y_n = cnn_prep(window_size, negative_dataset)

                train = np.vstack((train_p, train_n))
                train_y = np.hstack((train_y_p, train_y_n))
                validation = np.vstack((validation_p, validation_n))
                validation_y = np.hstack((validation_y_p, validation_y_n))
                test = np.vstack((test_p, test_n))
                test_y = np.hstack((test_y_p, test_y_n))

                print('Preparation for window_size of ', str(window_size), ' terminated !')
                print('Saving it ...')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), train, 'train')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), train_y, 'train_y')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), validation, 'validation')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), validation_y, 'validation_y')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), test, 'test')
                save_numpy(os.path.join(data_path, 'cnn_windows', str(window_size)), test_y, 'test_y')
