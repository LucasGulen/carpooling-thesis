from metier.travel import Travel
import pandas as pd
import os
import numpy as np
import Generative.config as config
from multiprocessing import Pool
import itertools


def __read_files(paths: list) -> [Travel]:
    dataset = []
    # Loop over each travel directory
    for path in paths:
        travel = Travel()
        file_paths = [e for e in path.iterdir() if e.is_file()]
        for file_path in file_paths:
            if ('positive' in str(file_path)):
                data = pd.read_csv(file_path, header=0)
                data['y'] = np.ones((data.shape[0],)).astype(int)
            elif ('negative' in str(file_path)):
                data = pd.read_csv(file_path, header=0)
                data['y'] = np.zeros((data.shape[0],)).astype(int)
            if ('driver' in str(file_path)):
                travel.driverData = data
            else:
                travel.passengersData.append(data)
        dataset.append(travel)
    return dataset


def __read_train_val_test(paths: list):
    train, validation, test = None, None, None
    for path in paths:
        travel = Travel()
        file_paths = [e for e in path.iterdir() if e.is_file()]
        for file_path in file_paths:
            tmp_data = pd.read_csv(file_path, header=0)
            if 'train' in str(file_path):
                if train is None:
                    train = tmp_data
                else:
                    train = np.vstack((train, tmp_data))
            elif 'test' in str(file_path):
                if test is None:
                    test = tmp_data
                else:
                    test = np.vstack((test, tmp_data))
            else:
                if validation is None:
                    validation = tmp_data
                else:
                    validation = np.vstack((validation, tmp_data))
    return train, validation, test


def read_data(path: str, model_load=False):
    # Find travel directories in path given in parameter
    travel_dir_paths = [e for e in path.iterdir() if e.is_dir()]
    # Read all files and return them as list of travel
    if model_load:
        train, validation, test = __read_train_val_test(travel_dir_paths)
        return train, validation, test
    else:
        data = __read_files(travel_dir_paths)
        return data


def __parallel_write_window_separate(travel, dir_name, data_path, sub_path, sub_sub_path):
    passenger = travel.passengerWindows
    driver = travel.driverWindows
    directory = os.path.join(data_path, sub_path, sub_sub_path, str(dir_name))
    ensure_dir(directory)
    pd.DataFrame(driver).to_csv(os.path.join(directory, 'driver.csv'), header=True, index=False)
    pd.DataFrame(passenger).to_csv(os.path.join(directory, 'passenger.csv'), header=True, index=False)

def __parallel_write_window_merge(travel, dir_name, data_path, sub_path, sub_sub_path):
    merge_d_p_window = np.array(travel.window_d_p_merged)
    directory = os.path.join(data_path, sub_path, sub_sub_path, str(dir_name))
    ensure_dir(directory)
    train_ratio = config.constants.get("SPLIT_RATIO")
    split_idx = int(train_ratio * merge_d_p_window.shape[0])

    # Split data into train 70% and test 30%
    train = merge_d_p_window[0:split_idx, :]
    test = merge_d_p_window[split_idx: merge_d_p_window.shape[0], :]

    # Take last 15% of test for validation split
    validation = test[test.shape[0] // 2: test.shape[0]]

    # Take first 15% of test for test data
    test = test[0:test.shape[0]//2]

    # Save train, validation and test data
    pd.DataFrame(train).to_csv(os.path.join(directory, 'train.csv'), header=True, index=False)
    pd.DataFrame(test).to_csv(os.path.join(directory, 'test.csv'), header=True, index=False)
    pd.DataFrame(validation).to_csv(os.path.join(directory, 'val.csv'), header=True, index=False)

def __save(data_path, dataset, sub_path, sub_sub_path, type):
    dir_name = 0

    # Multi-process saving variables
    agents = 20
    chunksize = 1
    data_paths, sub_paths, sub_sub_paths = itertools.repeat(data_path, len(dataset)), \
                                           itertools.repeat(sub_path, len(dataset)), \
                                           itertools.repeat(sub_sub_path, len(dataset))
    dataset_with_idx = zip(dataset, range(len(dataset)), data_paths, sub_paths, sub_sub_paths)

    if type == 'window':
        # Call function __parallel_write_window_separate with multiple process over dataset
        with Pool(processes=agents) as pool:
            pool.starmap(__parallel_write_window_separate, dataset_with_idx, chunksize)
    elif type == 'aligned':
        for travel in dataset:
            i = 0
            for passenger in travel.passengersAligned:
                driver = travel.driverAligned[i]
                directory = os.path.join(data_path, sub_path, sub_sub_path, str(dir_name))
                ensure_dir(directory)
                pd.DataFrame(driver).to_csv(os.path.join(directory, 'driver.csv'), header=True, index=False)
                pd.DataFrame(passenger).to_csv(os.path.join(directory, 'passenger.csv'), header=True, index=False)
                dir_name += 1
                i += 1
    elif type == 'window_d_p_merged':
        # Call function __parallel_write_window_merge with multiple process over dataset
        with Pool(processes=agents) as pool:
            pool.starmap(__parallel_write_window_merge, dataset_with_idx, chunksize)


def save_params(path, params: dict):
    ensure_dir(path)
    pd.DataFrame(params, index=[0]).to_csv(os.path.join(path, 'best_reg_params.csv'), header=True, index=False)


def save_data(data_path, pos_dataset, neg_dataset, sub_path, type):
    __save(data_path, pos_dataset, sub_path, 'positive_usecases', type)
    __save(data_path, neg_dataset, sub_path, 'negative_usecases', type)


def save_numpy(data_path, data, filename):
    directory = os.path.join(data_path)
    ensure_dir(directory)
    np.save(os.path.join(directory, filename), data)


def load_cnn_data(data_path, filename):
    return np.load(os.path.join(data_path, filename + '.npy'))

def save_dataframe(data_path, data, filename):
    directory = os.path.join(data_path)
    ensure_dir(directory)
    pd.DataFrame(data).to_csv(os.path.join(directory, filename + '.csv'), header=True, index=False)


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
