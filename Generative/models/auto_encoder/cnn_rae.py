from torch import nn
import torch
from torchsummary import summary



class CNN_RAE(nn.Module):
    def __init__(self, input_channels, trial):
        super(CNN_RAE, self).__init__()
        layers_encoder = []
        layers_decoder = []

        # Encoder (1, 70) batch_size x input_features
        layers_encoder.append(nn.Conv1d(input_channels, 64, kernel_size=1, padding=1, stride=1))
        layers_encoder.append(nn.ReLU(inplace=True))
        layers_encoder.append(nn.BatchNorm1d(64))

        layers_encoder.append(nn.Conv1d(64, input_channels//2, kernel_size=1, padding=1, stride=1))
        layers_encoder.append(nn.ReLU(inplace=True))
        layers_encoder.append(nn.BatchNorm1d(input_channels//2))
        layers_encoder.append(nn.MaxPool1d((1)))

        layers_encoder.append(nn.Conv1d(input_channels//2, input_channels//4, kernel_size=1, padding=1, stride=1))
        layers_encoder.append(nn.ReLU(inplace=True))
        layers_encoder.append(nn.BatchNorm1d(input_channels//4))
        layers_encoder.append(nn.MaxPool1d((1)))

        layers_encoder.append(nn.Conv1d(input_channels//4, 1, kernel_size=1, padding=1, stride=1))
        layers_encoder.append(nn.Linear(9, 16))
        layers_encoder.append(nn.BatchNorm1d(1))

        self.encoder = nn.Sequential(*layers_encoder)

        # Decoder

        layers_decoder.append(nn.Conv1d(1, input_channels//4, kernel_size=1, padding=1, stride=1))
        layers_decoder.append(nn.ReLU(inplace=True))
        layers_decoder.append(nn.BatchNorm1d(input_channels//4))

        layers_decoder.append(nn.Conv1d(input_channels//4, input_channels//2, kernel_size=1, padding=1, stride=1))
        layers_decoder.append(nn.ReLU(inplace=True))
        layers_decoder.append(nn.BatchNorm1d(input_channels//2))
        layers_decoder.append(nn.ConvTranspose1d(input_channels//2, input_channels//2, 1))

        layers_decoder.append(nn.Conv1d(input_channels//2, 64, kernel_size=1, padding=1, stride=1))
        layers_decoder.append(nn.ReLU(inplace=True))
        layers_decoder.append(nn.BatchNorm1d(64))

        layers_decoder.append(nn.ConvTranspose1d(64, 64, 1))

        layers_decoder.append(nn.Conv1d(64, input_channels, kernel_size=1, padding=1, stride=1))
        layers_decoder.append(nn.Linear(24, 1))


        self.decoder = nn.Sequential(*layers_decoder)

        # self.linear_encoder = nn.Linear(batch_size * output_channels, output_channels)
        # self.linear_decoder = nn.Linear(output_channels, batch_size * output_channels)

    def forward(self, x, only_encoder=False):
        if only_encoder:
            # (Batch_size, input_channels, input_size)
            # Add dimension to the end because conv1d layer need 3 dimensions and inputs have two
            x = x.unsqueeze(2)
            # Encoder
            x = self.encoder(x)
            x = x.squeeze(2)
        else:
            # (Batch_size, input_channels, input_size)
            # Add dimension to the end because conv1d layer need 3 dimensions and inputs have two
            x = x.unsqueeze(2)
            # Encoder
            # print(self.encoder)
            x = self.encoder(x)

            # Decode
            # print(self.decoder)
            x = self.decoder(x)
            x = x.squeeze(2)
        return x

    def output_shape(self, input_shape, padding, kernel_size, stride, dilation=1):
        # Pytorch documentation formula
        return int(((input_shape + 2 * padding - dilation * (kernel_size - 1) - 1) / stride) + 1)
