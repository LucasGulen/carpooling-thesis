from torch import nn


class CNN(nn.Module):
    def __init__(self, input_channels, params):
        super(CNN, self).__init__()
        # output_channels = trial.suggest_int("n_output_channels", 1, 64)
        output_channels = input_channels // 2
        conv_out_1 = int(self.output_shape(output_channels, 1, 1, 1))
        conv_out_2 = int(self.output_shape(conv_out_1, 1, 1, 1))

        # Encoder 2 convolution layers with batchnorm and Relu activation followed by a final maxpool layer
        self.encoder = nn.Sequential(
            nn.Conv1d(input_channels, output_channels, kernel_size=1, padding=1, stride=1),
            nn.BatchNorm1d(output_channels),
            nn.ReLU(inplace=True),
            nn.Dropout(params['dropout']),
            nn.Conv1d(output_channels, output_channels//4, kernel_size=1, padding=1, stride=1),
            nn.BatchNorm1d(output_channels//4),
            nn.ReLU(inplace=True),
            nn.Dropout(params['dropout']),
        )

        # Decoder (ConvTranspose layer is a deconvolution layer)
        self.decoder = nn.Sequential(
            nn.ConvTranspose1d(output_channels//4, output_channels, kernel_size=1, stride=1, padding=1),
            nn.ConvTranspose1d(output_channels, input_channels, kernel_size=1, stride=1, padding=1)
        )

        # self.linear_encoder = nn.Linear(batch_size * output_channels, output_channels)
        # self.linear_decoder = nn.Linear(output_channels, batch_size * output_channels)

    def forward(self, x, only_encoder=False):
        if only_encoder:
            # (Batch_size, input_channels, input_size)
            # Add dimension to the end because conv1d layer need 3 dimensions and inputs have two
            x = x.unsqueeze(2)
            # Encoder
            x = self.encoder(x)
            x = x.squeeze(2)
        else:
            # (Batch_size, input_channels, input_size)
            # Add dimension to the end because conv1d layer need 3 dimensions and inputs have two
            x = x.unsqueeze(2)
            # Encoder
            x = self.encoder(x)

            # Decode
            x = self.decoder(x)
            x = x.squeeze(2)
        return x

    def output_shape(self, input_shape, padding, kernel_size, stride, dilation=1):
        # Pytorch documentation formula
        return ((input_shape + 2 * padding - dilation * (kernel_size - 1) - 1) / stride) + 1
