from torch import nn


class MLP(nn.Module):
    def __init__(self, input_shape, model_params):
        super(MLP, self).__init__()

        self.encoder = nn.Sequential(
            nn.Linear(input_shape, input_shape//2),
            nn.ReLU(True),
            nn.Linear(input_shape//2, input_shape//4),
            nn.ReLU(True),
            nn.Linear(input_shape//4, input_shape))
        self.decoder = nn.Sequential(
            nn.Linear(input_shape, input_shape//4),
            nn.ReLU(True),
            nn.Linear(input_shape//4, input_shape//2),
            nn.ReLU(True),
            nn.Linear(input_shape//2, input_shape))


    def forward(self, x, only_encoder=False):
        if only_encoder:
            # Encoder
            x = self.encoder(x)
        else:
            # Encoder
            x = self.encoder(x)

            # Decode
            x = self.decoder(x)
        return x
