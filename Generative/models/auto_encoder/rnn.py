import torch.nn as nn
import torch
from torch.autograd import Variable
import numpy as np

class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(Encoder, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        self.rnn = nn.RNN(input_size, hidden_size, num_layers, batch_first=True)
        self.initiliate_weights()

    def forward(self, x):
        h0 = self.init_hidden(x)
        encoded_input, _ = self.rnn(x, h0.detach())
        return encoded_input

    def init_hidden(self, x):
        # Pytorch documentation says that shape of h0 and c0 is (num_layers x batch_size x hidden_size)
        # It's common to initialize them to set of zero
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).cuda()
        return h0

    def initiliate_weights(self):
        # initialize weights
        nn.init.xavier_uniform(self.rnn.weight_ih_l0, gain=np.sqrt(2))
        nn.init.xavier_uniform(self.rnn.weight_hh_l0, gain=np.sqrt(2))


class Decoder(nn.Module):
    def __init__(self, hidden_size, output_size, num_layers):
        super(Decoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.num_layers = num_layers

        self.rnn = nn.RNN(hidden_size, output_size, num_layers, batch_first=True)
        self.initialize_weights()

        self.fc = nn.Linear(output_size, output_size)

    def forward(self, x_encoded):
        h0 = self.init_hidden(x_encoded)
        decoded_output, _ = self.rnn(x_encoded, h0.detach())
        return self.fc(decoded_output)

    def init_hidden(self, x):
        # Pytorch documentation says that shape of h0 and c0 is (num_layers x batch_size x hidden_size)
        # It's common to initialize them to set of zero
        h0 = torch.zeros(self.num_layers, x.size(0), self.output_size).cuda()
        return h0

    def initialize_weights(self):
        # initialize weights
        nn.init.xavier_uniform(self.rnn.weight_ih_l0, gain=np.sqrt(2))
        nn.init.xavier_uniform(self.rnn.weight_hh_l0, gain=np.sqrt(2))

class RNNAE(nn.Module):
    def __init__(self, input_size, seq_size, params):
        super(RNNAE, self).__init__()

        self.params = params
        self.seq_size = seq_size
        self.encoder = Encoder(input_size, params['hidden_size'], params['num_layers'])
        self.decoder = Decoder(params['hidden_size'], input_size, params['num_layers'])

    def forward(self, x, only_encoder=False):
        if only_encoder :
            return self.encoder(x)[:, -1, :]
        else:
            x_encoded = self.encoder(x)
            return self.decoder(x_encoded)
