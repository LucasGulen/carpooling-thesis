from optuna import Trial
from torch import nn
import torch
import torch.nn.functional as F

class MLPVAE(nn.Module):
    def __init__(self, input_shape, trial: Trial):
        super(MLPVAE, self).__init__()

        self.input_shape = input_shape
        self.fc1 = nn.Linear(input_shape, input_shape//2)
        self.fc1_bn = nn.BatchNorm1d(input_shape//2)

        self.mu_layer = nn.Linear(input_shape//2, input_shape//4)
        self.std_layer = nn.Linear(input_shape//2, input_shape//4)

        self.fc3 = nn.Linear(input_shape//4, input_shape//2)
        self.fc3_bn = nn.BatchNorm1d(input_shape//2)
        self.fc4 = nn.Linear(input_shape//2, input_shape)

    def encode(self, x):
        h1 = F.relu(self.fc1_bn(self.fc1(x)))
        return self.mu_layer(h1), self.std_layer(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        sample = mu + eps*std
        return sample

    def decode(self, z):
        h3 = F.relu(self.fc3_bn(self.fc3(z)))
        return self.fc4(h3)

    def forward(self, x, only_encoder=False):
        mu, logvar = self.encode(x.view(-1, self.input_shape))
        z = self.reparameterize(mu, logvar)
        if only_encoder:
            return z, mu, logvar
        else:
            return self.decode(z), mu, logvar
