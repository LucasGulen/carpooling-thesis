import os

from torch.autograd import Variable

import Generative.config as config
from Generative.data_prep_AE.utils import prepare_encoder_data
from Generative.models.auto_encoder.mlp import MLP
from Generative.models.auto_encoder.cnn import CNN
from Generative.models.auto_encoder.lstm import LSTMAE
from Generative.models.auto_encoder.rnn import RNNAE
from Generative.models.auto_encoder.cnn_rae import CNN_RAE
from Generative.models.variational_auto_encoder.mlp import MLPVAE
from Generative.utils import get_best_mse, save_mse
from Classifier.utils import prepare_data_loader, read_train_val_test_data
from Generative.data_preparation.utils import read_data, save_dataframe
from torch import nn
import torch
import optuna
from optuna import Trial
import torch.optim as optim
import numpy as np
import sklearn
from pathlib import Path
import pandas as pd


N_EPOCHS = 50
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
base_path = os.path.join(Path.cwd(), 'Generative', 'models')


class Objective(object):
    def __init__(self, train_data, validation_data, test_data, model_class_name, model_name, window_size, model_dir):
        self.model_dir = model_dir
        self.train_data = train_data
        self.validation_data = validation_data
        self.test_data = test_data
        self.model_class_name = model_class_name
        self.model_name = model_name
        self.window_size = window_size

    def __call__(self, trial: Trial):
        '''
        if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
            if self.window_size <= 55:
                batch_size = trial.suggest_int('batch_size', 128, 2048, step=32)
            if self.window_size == 89:
                batch_size = trial.suggest_int('batch_size', 128, 1512, step=32)
        '''

        batch_size = trial.suggest_int('batch_size', 128, 512, step=32)

        learning_rate = trial.suggest_loguniform('lr', 1e-4, 1e-1)
        trial.suggest_uniform("dropout", 1e-4, 3e-1)

        mean_train = np.mean(self.train_data[:, 0:self.train_data.shape[1]-1], axis=0)
        std_train = np.std(self.train_data[:, 0:self.train_data.shape[1]-1], axis=0)

        # Standardization
        self.train_data[:, 0:self.train_data.shape[1]-1] = (self.train_data[:, 0:self.train_data.shape[1]-1] - mean_train) / std_train
        self.validation_data[:, 0:self.validation_data.shape[1]-1] = (self.validation_data[:, 0:self.validation_data.shape[1]-1] - mean_train) / std_train
        self.test_data[:, 0:self.test_data.shape[1]-1] = (self.test_data[:, 0:self.test_data.shape[1]-1] - mean_train) / std_train

        if self.model_class_name == 'MLPVAE':
            path = os.path.join(base_path, 'variational_auto_encoder', 'best_' + self.model_name + '_params')
        else:
            path = os.path.join(base_path, 'auto_encoder', 'best_' + self.model_name + '_params')

        np.save(os.path.join(path, 'mean_train_' + str(self.window_size)), mean_train)
        np.save(os.path.join(path, 'std_train_' + str(self.window_size)), std_train)

        mean_train = torch.FloatTensor(mean_train).to(DEVICE)
        std_train = torch.FloatTensor(std_train).to(DEVICE)

        train_loader, validation_loader, test_loader = prepare_encoder_data(batch_size, self.window_size,
                                                                            self.model_name, self.train_data,
                                                                            self.validation_data, self.test_data)
        # torch.cuda.empty_cache()

        # Instantiate model with parameters
        trial, model = self.set_params(trial)

        if self.model_class_name == 'MLPVAE':
            criterion = nn.MSELoss(reduction='none')
        else:
            criterion = nn.MSELoss()
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)

        found_best_model = False
        min_loss = 10000000
        cpt_early_stop = 0
        loss_list = []
        rmse_list = []
        print('################################')
        print(self.model_name + ' training with params : ', trial.params)
        print('################################')
        for epoch in range(N_EPOCHS):  # loop over the dataset multiple times
            model.train()
            training_loss = 0
            # TRAINING ROUND
            for train_inputs, labels in train_loader:
                # zero the parameter gradients
                optimizer.zero_grad()

                inputs = train_inputs.float()

                if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
                    self.INPUTS_DIM = 1
                    inputs = inputs.view(-1, self.SEQ_DIM, self.INPUTS_DIM)

                inputs, labels = inputs.to(DEVICE), labels.to(device=DEVICE, dtype=torch.float)

                if self.model_class_name == 'MLPVAE':
                    # rmse_loss = criterion(outputs, labels)

                    outputs, mu, logvar = model(inputs)
                    # Un-standardize to calculate loss on original data scale
                    loss_per_feature = criterion(outputs * std_train + mean_train, labels * std_train + mean_train).mean(axis=0)
                    rmse_loss = torch.sqrt(torch.mean(loss_per_feature))
                    train_loss = vae_loss(rmse_loss, mu, logvar)
                    training_loss += train_loss
                else:
                    # forward + backward + optimize
                    outputs = model(inputs)
                    if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
                        outputs = outputs.squeeze(-1)
                    train_loss = torch.sqrt(criterion(outputs * std_train + mean_train, labels * std_train + mean_train))
                    training_loss += train_loss

                train_loss.cuda()
                train_loss.backward()
                with torch.no_grad():
                    optimizer.step()
            training_loss = training_loss / len(train_loader)
            # Calculate accuracy on validation set after each epoch
            with torch.set_grad_enabled(False):
                running_loss = 0
                model.eval()
                true_labels_lst = []
                preds_lst = []
                # Iterate through test dataset
                for val_inputs, val_labels in validation_loader:
                    val_inputs = val_inputs.float()
                    val_inputs, labels = val_inputs.cuda(), val_labels.to(device='cuda:0', dtype=torch.float)

                    if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
                        val_inputs = val_inputs.view(-1, self.SEQ_DIM, self.INPUTS_DIM)

                    # Forward propagation
                    outputs = model(val_inputs)

                    if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
                        outputs = outputs.squeeze(-1)

                    if self.model_class_name == 'MLPVAE':
                        reconstruction, mu, logvar = model(val_inputs)

                        # Un-standardize to calculate loss on original data scale
                        loss_per_feature = criterion(reconstruction * std_train + mean_train, labels * std_train + mean_train).mean(axis=0)
                        rmse_loss = torch.sqrt(torch.mean(loss_per_feature))
                        loss = vae_loss(rmse_loss, mu, logvar)

                        running_loss += loss.item()
                    else:
                        # Get predictions from the maximum value
                        preds_lst.append(outputs.data.cpu().numpy())
                    true_labels_lst.append(val_labels.cpu().numpy())

                if self.model_class_name == 'MLPVAE':
                    loss = torch.FloatTensor(np.array(running_loss / len(validation_loader)))
                else:
                    # Transform true label list to float tensor
                    true_labels = torch.FloatTensor(true_labels_lst)
                    true_labels = torch.reshape(true_labels, (true_labels.size(1) * true_labels.size(0), true_labels.size(2)))

                    # Transform preds list to float tensor
                    preds = torch.FloatTensor(preds_lst)
                    preds = torch.reshape(preds, (preds.size(1) * preds.size(0), preds.size(2)))

                    # Calculate loss on validation
                    loss = torch.sqrt(criterion(preds * std_train.cpu() + mean_train.cpu(), true_labels * std_train.cpu() + mean_train.cpu()))

                # Early stopping if validation accuracy didn't increase in last 10 epochs
                if loss >= min_loss:
                    cpt_early_stop += 1
                    if cpt_early_stop == 10:
                        trial.report(min_loss, epoch)
                        if found_best_model:
                            # Before exiting the method, save the loss history if best model founded
                            save_loss_history(loss_list, rmse_list, os.path.join(base_path, self.model_dir, 'best_' + self.model_name + '_params'), self.window_size)
                        return min_loss
                else:
                    min_loss = loss.data.item()
                    cpt_early_stop = 0
                    if min_loss < get_best_mse(os.path.join(base_path, self.model_dir, 'best_' + self.model_name + '_params', 'best_mse_' + str(self.window_size) + '.txt')):
                        found_best_model = True

                        # Save best mse
                        save_mse(loss.data.item(), os.path.join(base_path, self.model_dir, 'best_' + self.model_name
                                                         + '_params', "best_mse_" + str(self.window_size) + ".txt"))
                        # Save best model
                        torch.save(model, os.path.join(base_path, self.model_dir,
                                                       'best_' + self.model_name + '_params', 'model_' + str(self.window_size) + '.pth'))

                # store loss and mse (can be useful for plot)
                loss_list.append(training_loss.item())
                rmse_list.append(loss.data.item())
                # Print Loss on train and accuracy on test each epoch
                print(
                    'Epoch: {}  Loss on train: {}  MSE on validation set: {} '.format(epoch + 1, training_loss.data.item(), loss.data.item()))

        # if best model founded, save loss history
        if found_best_model:
            save_loss_history(loss_list, rmse_list, os.path.join(base_path, self.model_dir, 'best_' + self.model_name + '_params'), self.window_size)

        trial.report(np.min(rmse_list), epoch)
        return np.min(rmse_list)

    def set_batch_hidden_size(self, seq_dim, trial: Trial):
        if self.window_size == 34 or self.window_size == 55:
            trial.suggest_int('hidden_size', seq_dim // 2, seq_dim // 2)
        return trial

    def set_params(self, trial: Trial):
        model_class = globals()[self.model_class_name]
        if self.model_class_name == 'RNNAE' or self.model_class_name == 'LSTMAE':
            self.SEQ_DIM = self.window_size * 7 * 2
            # trial = self.set_batch_hidden_size(self.SEQ_DIM, trial)
            trial.suggest_int('hidden_size', self.SEQ_DIM // 2, self.SEQ_DIM // 2)
            trial.suggest_int('num_layers', 1, 2, 3)
            model_params = {key: value for key, value in trial.params.items() if
                            key != 'batch_size' and key != 'lr'}
            seq_size = self.window_size * 7 * 2
            model = model_class(1, seq_size, model_params)
        else:
            model_params = {key: value for key, value in trial.params.items() if
                            key != 'batch_size' and key != 'lr'}
            model = model_class(self.window_size * 7 * 2, model_params)
        model.to(DEVICE)

        return trial, model


def save_loss_history(train_loss: list, validation_loss: list, directory, window_size):
    validation = np.concatenate((['validation_loss'], validation_loss))
    train = np.concatenate((['train_loss'], train_loss))
    dataset = pd.DataFrame(np.vstack((train, validation)))
    pd.DataFrame(dataset).to_csv(os.path.join(directory, 'val_train_loss_' + str(window_size) + '.csv'), header=False, index=False)


def vae_loss(bce_loss, mu, logvar):
    """
    Function from pytorch documentation on VAE
    This function will add the reconstruction loss (BCELoss) and the
    KL-Divergence.
    KL-Divergence = 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)

    :param bce_loss: recontruction loss
    :param mu: the mean from the latent vector
    :param logvar: log variance from the latent vector
    """

    KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())

    return bce_loss + KLD

def nn_train(model_type, model_name, window_size_lst, model_dir):
    for window_size in window_size_lst:
        # Load data
        data_path_positive = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'positive_usecases'))
        data_path_negative = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'negative_usecases'))

        train_data, validation_data, test_data = read_train_val_test_data(data_path_positive, data_path_negative)

        # Create optuna hyperparameter tuning instance
        sampler = optuna.samplers.TPESampler()

        study = optuna.create_study(direction="minimize", sampler=sampler, study_name=model_name + '_' + str(window_size) + '_study',
                                    storage='sqlite:///Generative/models/' + model_dir + '/best_' + model_name + '_params/' + model_name
                                            + '_' + str(window_size) + '_study.db',
                                    load_if_exists=True)

        study.optimize(Objective(train_data, validation_data, test_data, model_type, model_name, window_size, model_dir), n_trials=1, show_progress_bar=True)

        # Get best accuracy
        print('Best accuracy on ', model_name, '(window of ', str(window_size), ')', ' was : ', study.best_value,
              ' with params : ', study.best_params)

        # Save optuna models history
        study_df = study.trials_dataframe()
        save_dataframe(os.path.join(Path.cwd(), 'Generative', 'models', model_dir, 'studies'), study_df,
                       model_name + str(window_size) + '_optuna_study')


# window_size_lst = config.constants.get("WINDOW_SIZE_LST")
# nn_train('AutoEncoder', 'mlp', window_size_lst)
