import re
import os
import json

def get_best_mse(path):
    if not os.path.exists(path):
        mse_file = open(path, 'w+')
        mse_file.write('10000000')
        mse_file.close()
        return 10000000
    else:
        with open(path, "r+") as mse_file:
            best_mse = float(mse_file.read())
    return best_mse

# Save given mse only if it's lower than the best one previously stored
def save_mse(mse, path):
    if not os.path.exists(path):
        mse_file = open(path, 'w+')
        mse_file.write('10000000')
        mse_file.close()
    else:
        with open(path, "r+") as best_mse_file:
            best_mse = best_mse_file.read()
            if mse < float(best_mse):
                replace_txt = re.sub(best_mse, str(mse), best_mse)
                best_mse_file.seek(0)
                best_mse_file.write(replace_txt)
                best_mse_file.truncate()


def save_metric(metric_value, path):
    metric = open(path, 'w+')
    metric.write(json.dumps(metric_value.tolist()))
    metric.close()


def read_metric(path):
    with open(path, "r+") as metric_file:
        metric = metric_file.read()
    metric = json.loads(metric)
    return metric
