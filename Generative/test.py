import os
from pathlib import Path
import sklearn
from scipy.fftpack import fft
from sklearn.metrics import mean_squared_error
import torch
import numpy as np
import Generative.config as config
import matplotlib.pyplot as plt
import warnings
from Classifier.utils import read_train_val_test_data
from sklearn.decomposition import PCA
import seaborn as sns
import torch.nn.functional as F


warnings.filterwarnings("ignore")
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
base_path = os.path.join(Path.cwd(), 'Generative', 'models')


def test_data_on_classifier(data, window_size, y_test, data_type, classifier_name, auto_encoder_name):
    # Load best classifier
    rnn = torch.load(os.path.join(Path.cwd(), 'Classifier', 'models', 'best_' + classifier_name + '_params',
                                  'model_' + str(5) + '.pth'))
    rnn.eval()
    x_test = torch.Tensor(data).float().view(-1, window_size * 7 * 2, 1).cuda()
    outputs = rnn(x_test)
    predicted = torch.max(outputs.data, 1)[1].cpu().numpy()

    f1_score = sklearn.metrics.f1_score(y_test, predicted, average='binary') * 100
    recall = sklearn.metrics.recall_score(y_test, predicted, average='binary') * 100
    precision = sklearn.metrics.precision_score(y_test, predicted, average='binary') * 100
    accuracy = sklearn.metrics.accuracy_score(y_test, predicted) * 100

    print('\n###Results ', classifier_name + '-Classifier, ' + auto_encoder_name + '-AutoEncoder, ',
          data_type + '-data###')
    print('F1 score :', str(f1_score))
    print('Recall score :', str(recall))
    print('Precision score :', str(precision))
    print('Accuracy score :', str(accuracy))


def scatter_plots(decoded_1, decoded_2, encoded_1, encoded_2, raw_1, raw_2, filename, sensor_type):
    print('Generating plot ...')
    plt.scatter(raw_1, raw_2, c='green', alpha=0.5, label='Raw', edgecolor='none')
    plt.scatter(decoded_1, decoded_2, c='red', alpha=0.5, label='Encoded-Decoded', edgecolor='none')
    plt.scatter(encoded_1, encoded_2, c='blue', alpha=0.5, label='Encoded', edgecolor='none')
    plt.title('Scatter plot between cnn-decoded, raw and encoded')
    plt.xlabel(sensor_type + '_component_1')
    plt.ylabel(sensor_type + '_component_2')
    plt.legend()
    print('Saving plot ...')
    plt.savefig(os.path.join(Path.cwd(), 'AutoEncoder', 'models', 'plots', filename))
    plt.close()

def standardize(mean, std, data):
    return (data - mean) / std


def autoencoder_test(test, window_size, model_name: str):
    if 'vae' in model_name:
        model_type = 'variational_auto_encoder'
    else:
        model_type = 'auto_encoder'

    # Standardize data
    mean = torch.FloatTensor(np.load(os.path.join(base_path, model_type, 'best_' + model_name + '_params', 'mean_train_' + str(window_size) + '.npy'))).to(DEVICE)
    std = torch.FloatTensor(np.load(os.path.join(base_path, model_type, 'best_' + model_name + '_params', 'std_train_' + str(window_size) + '.npy'))).to(DEVICE)
    test_standardized = standardize(mean, std, test)

    # Load model in eval mode
    model = torch.load(
        os.path.join(base_path, model_type, 'best_' + model_name + '_params', 'model_' + str(window_size) + '.pth'))
    model.eval()

    if 'rnn' in model_name or 'lstm' in model_name:
        inputs = test_standardized.view(-1, window_size * 7 * 2, 1)
    else:
        inputs = test_standardized

    if 'vae' in model_name:
        # Run only encoder part to get the z latent space
        z, mu, logvar = model.forward(inputs, only_encoder=True)
        # Run encoder and decoder part
        x_decoded, _, _ = model(inputs)
        z = z.data.cpu()
        x_decoded = x_decoded.data.cpu()
    else:
        # Run only encoder part to get the z latent space
        z = model.forward(inputs, only_encoder=True).data.cpu()
        # Run encoder and decoder part
        x_decoded = model(inputs)
        x_decoded = x_decoded.data.cpu()

    # Compute rmse on unstandardized data to see the real error
    if 'rnn' in model_name or 'lstm' in model_name:
        x_decoded = x_decoded.squeeze(-1) * std.cpu() + mean.cpu()
        mse = mean_squared_error(test_standardized.cpu() * std.cpu() + mean.cpu(), x_decoded)
    else:
        x_decoded = x_decoded * std.cpu() + mean.cpu()
        mse = mean_squared_error(test_standardized.cpu() * std.cpu() + mean.cpu(), x_decoded)
    rmse = np.sqrt(mse)

    print('RMSE for ' + model_name.upper() + ' on test data : ', rmse)

    return z, x_decoded, rmse, mean, std


def pca_reduction(decoded, encoded, mean, std,raw, window_size, nb_components, model_name: str):
    # This show density probability function on data
    # Create PCA model
    pca = PCA(n_components=nb_components, svd_solver='randomized')

    # Standardize data before PCA
    decoded = (decoded.cpu() - mean.cpu()) / std.cpu()
    raw = (raw.cpu() - mean.cpu()) / std.cpu()

    # Use PCA to have only 1 component by sensor
    pca_raw = pca.fit_transform(raw).reshape(-1)
    pca_encoded = pca.fit_transform(encoded).reshape(-1)
    pca_decoded = pca.fit_transform(decoded).reshape(-1)

    # Create and save a probability density plot with each data distributions
    fig = plt.figure()
    sns.set_style('whitegrid')
    sns.kdeplot(np.array(pca_raw), bw=0.5, shade=True)
    sns.kdeplot(np.array(pca_decoded), bw=0.5, shade=True)
    sns.kdeplot(np.array(pca_encoded), bw=0.5, shade=True)
    plt.legend(np.array(['x', 'x\'-' + model_name.upper(), 'z-' + model_name.upper()]), ncol=2, loc='upper right')
    plt.ylabel('$P(x)$')
    plt.xlabel('Component values')
    # plt.title('Probability density plot of 1-components distributions (w_size=' + str(window_size) + ')')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'results', 'pdfplot' + model_name.upper() + '_' + str(window_size) + '.svg'), format='svg')


    # This show scatter plots
    # Create PCA model
    pca = PCA(n_components=2, svd_solver='randomized')

    # Use PCA to have only 1 component by sensor
    pca_raw = pca.fit_transform(raw)
    pca_encoded = pca.fit_transform(encoded)
    pca_decoded = pca.fit_transform(decoded)

    # Create and save a scatter plot with each data distributions
    fig = plt.figure()
    sns.set_style('whitegrid')
    sns.scatterplot(np.array(pca_raw[:, 0]), np.array(pca_raw[:, 1]), marker='+', alpha=0.5)
    sns.scatterplot(np.array(pca_decoded[:, 0]), np.array(pca_decoded[:, 1]), marker='+', alpha=0.5)
    sns.scatterplot(np.array(pca_encoded[:, 0]), np.array(pca_encoded[:, 1]), marker='+', alpha=0.5)
    plt.legend(np.array(['x', 'x\'-' + model_name.upper(), 'z-' + model_name.upper()]), loc='upper right')
    # plt.title('Scatter plot of 2-components distributions (w_size=' + str(window_size) + ')')
    plt.ylabel('Component 1')
    plt.xlabel('Component 2')
    plt.tight_layout()
    fig.savefig(os.path.join(Path.cwd(), 'Descriptive_analysis', 'results', 'scatterplot_' + model_name.upper() + '_' + str(window_size) + '.svg'), format='svg')
    '''
    # Extract all 3-axis for each sensor
    decoded_accelerometer = decoded[:, [0, window_size, window_size * 2]]
    decoded_magnetometer = decoded[:, [window_size * 3, window_size * 4, window_size * 5]]

    encoded_accelerometer = encoded[:, [0, window_size, window_size * 2]]
    encoded_magnetometer = encoded[:, [window_size * 3, window_size * 4, window_size * 5]]

    raw_accelerometer = raw[:, [0, window_size, window_size * 2]]
    raw_magnetometer = raw[:, [window_size * 3, window_size * 4, window_size * 5]]

    # Use PCA to have only 2 components by sensor to be able to plot each one in separate graph later
    pca_decoded_acc = pca.fit_transform(decoded_accelerometer)
    pca_decoded_magn = pca.fit_transform(decoded_magnetometer)

    pca_encoded_acc = pca.fit_transform(encoded_accelerometer)
    pca_encoded_magn = pca.fit_transform(encoded_magnetometer)

    pca_raw_acc = pca.fit_transform(raw_accelerometer.cpu())
    pca_raw_magn = pca.fit_transform(raw_magnetometer.cpu())
    '''
    return pca


def plots(decoded_acc, decoded_magn, encoded_acc, encoded_magn, raw_acc, raw_magn):
    plt.plot(decoded_acc, c='red', label='Decoded', alpha=0.5)
    plt.plot(raw_acc, c='green', label='Raw', alpha=0.5)
    plt.plot(encoded_acc, c='blue', label='Encoded', alpha=0.5)
    plt.title('Accelerometer')
    plt.xlabel('Frames (1=500ms)')
    plt.ylabel('PCA component values')
    plt.legend()
    plt.show()

    plt.plot(decoded_magn, c='red', label='Decoded', alpha=0.5)
    plt.plot(raw_magn, c='green', label='Raw', alpha=0.5)
    plt.plot(encoded_magn, c='blue', label='Encoded', alpha=0.5)
    plt.title('Magnetometer')
    plt.xlabel('Frames (1=500ms)')
    plt.ylabel('PCA component values')
    plt.legend()
    plt.show()


def test():
    window_size_lst = [5, 8, 13, 21]
    for window_size in window_size_lst:
        # Load data for MLP and CNN Auto Encoder
        data_path_positive = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'positive_usecases'))
        data_path_negative = Path(
            os.path.join(config.constants.get("ORIGINAL_BASE_PATH_WINDOWS_ACCELERO_MAGNETO"), str(window_size),
                         'window_merged', 'negative_usecases'))

        _, _, test_data = read_train_val_test_data(data_path_positive, data_path_negative)
        y_test = test_data[:, test_data.shape[1] - 1]
        test = np.delete(test_data, test_data.shape[1] - 1, 1)
        test = torch.Tensor(test).to(DEVICE)

        # Compute all autoencoders
        print('Window size ', str(window_size), ' : ')
        z_mlp, x_decoded_mlp, rmse_mlp, mean_mlp, std_mlp = autoencoder_test(test, window_size, 'mlp')
        z_cnn, x_decoded_cnn, rmse_cnn, mean_cnn, std_cnn = autoencoder_test(test, window_size, 'cnn')
        z_rnn, x_decoded_rnn, rmse_rnn, mean_rnn, std_rnn = autoencoder_test(test, window_size, 'rnnae')
        z_lstm, x_decoded_lstm, rmse_lstm, mean_lstm, std_lstm = autoencoder_test(test, window_size, 'lstmae')
        z_mlp_vae, x_decoded_mlp_vae, rmse_mlp_vae, mean_mlp_vae, std_mlp_vae = autoencoder_test(test, window_size, 'mlpvae')


        # Use PCA to reduce dimensions to be able to make one scatter plots on magnetometer et one on accelerometer
        pca = pca_reduction(x_decoded_mlp, z_mlp, mean_mlp, std_mlp, test, window_size, 1, 'mlp')
        # pca = pca_reduction(x_decoded_cnn, z_cnn, mean_cnn, std_cnn, test, window_size, 1)
        pca = pca_reduction(x_decoded_rnn, z_rnn, mean_rnn, std_rnn, test, window_size, 1, 'rnn')
        pca = pca_reduction(x_decoded_lstm, z_lstm, mean_lstm, std_lstm, test, window_size, 1, 'lstm')
        pca = pca_reduction(x_decoded_mlp_vae, z_mlp_vae, mean_mlp_vae, std_mlp_vae, test, window_size, 1, 'mlp-vae')


        '''
        scatter_plots(decoded_acc[:, 0], decoded_acc[:, 1], encoded_acc[:, 0], encoded_acc[:, 1],
                      raw_acc[:, 0], raw_acc[:, 1], 'acc_raw_encoded_decoded.png', 'Accelerometer')
        scatter_plots(decoded_magn[:, 0], decoded_magn[:, 1], encoded_magn[:, 0], encoded_magn[:, 1],
                      raw_magn[:, 0], raw_magn[:, 1], 'magn_raw_encoded_decoded.png', 'Magnetometer')

        # Use PCA to reduce to one components by sensor
        decoded_acc, decoded_magn, encoded_acc, encoded_magn, raw_acc, raw_magn = pca_reduction(decoded_mlp, encoded_mlp, test, window_size, 1)

        plots(decoded_acc, decoded_magn, encoded_acc, encoded_magn, raw_acc, raw_magn)

        # Compute RNN classifier on encoded and decoded data
        test_data_on_classifier(encoded_mlp, window_size, y_test, data_type='Encoded', classifier_name='rnn', auto_encoder_name='mlp')
        test_data_on_classifier(decoded_mlp, window_size, y_test, data_type='Decoded', classifier_name='rnn', auto_encoder_name='mlp')
        # test_data_on_classifier(encoded_cnn, window_size, y_test, data_type='Encoded', classifier_name='rnn', auto_encoder_name='cnn')
        test_data_on_classifier(decoded_cnn, window_size, y_test, data_type='Decoded', classifier_name='rnn', auto_encoder_name='cnn')
        # Compute LSTM classifier on encoded and decoded data
        test_data_on_classifier(encoded_mlp, window_size, y_test, data_type='Encoded', classifier_name='lstm', auto_encoder_name='mlp')
        test_data_on_classifier(decoded_mlp, window_size, y_test, data_type='Decoded', classifier_name='lstm', auto_encoder_name='mlp')
        # test_data_on_classifier(encoded_cnn, window_size, y_test, data_type='Encoded', classifier_name='lstm', auto_encoder_name='cnn')
        test_data_on_classifier(decoded_cnn, window_size, y_test, data_type='Decoded', classifier_name='lstm', auto_encoder_name='cnn')
        '''


if __name__ == '__main__':
    test()
