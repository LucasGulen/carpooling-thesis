import numpy as np
import Generative.config as config
import torch
import sys
import os
from pathlib import Path

sys.path.insert(0, os.getcwd())

base_path = os.path.join(Path.cwd(), 'AutoEncoder', 'models')

def prepare_encoder_data (batch_size, window_size, model_name, train_data, validation_data, test_data):
    # Delete labels
    train = np.delete(train_data, train_data.shape[1] - 1, 1)
    validation = np.delete(validation_data, validation_data.shape[1] - 1, 1)
    test = np.delete(test_data, test_data.shape[1] - 1, 1)

    '''
    # Normalize data
    mean_train = np.mean(train, axis=0)
    std_train = np.std(train, axis=0)

    # Save mean and std for futur normalization
    save_metric(mean_train, os.path.join(base_path, 'best_' + model_name + '_params', 'mean_' + str(window_size) + '.txt'))
    save_metric(std_train, os.path.join(base_path, 'best_' + model_name + '_params', 'std_' + str(window_size) + '.txt'))

    train = (train - mean_train) / std_train
    validation = (validation - mean_train) / std_train
    test = (test- mean_train) / std_train
    '''

    train_lst = []
    for i in range(len(train)):
        train_lst.append([train[i], train[i]])

    validation_lst = []
    for i in range(len(validation)):
        validation_lst.append([validation[i], validation[i]])

    test_lst = []
    for i in range(len(test)):
        test_lst.append([test[i], test[i]])

    # prepare data in batch (needed by pytorch)
    train_loader = torch.utils.data.DataLoader(train_lst, batch_size=batch_size,
                                               shuffle=False, num_workers=0, drop_last=True, pin_memory=True)

    validation_loader = torch.utils.data.DataLoader(validation_lst, batch_size=batch_size,
                                                    shuffle=False, num_workers=0, drop_last=True, pin_memory=True)

    test_loader = torch.utils.data.DataLoader(test_lst, batch_size=batch_size,
                                              shuffle=False, num_workers=0, drop_last=True, pin_memory=True)

    # Return train, validation and test dataloader
    return train_loader, validation_loader, test_loader
