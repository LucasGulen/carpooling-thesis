import inquirer
from Generative import config as config
from Generative.test import test
from Generative.train import nn_train
import sys
# Add root path to Pythonpath (allow to execute script in command line too)
sys.path.append('./')

# Define static list of parameters that can be choosen by the user with the prompt for training
nn_model_types = [
    inquirer.List('NN_model_type',
                  message="Which NN model type do you want to train for the auto-encoder ?",
                  choices=['MLP', 'CNN', 'RNN', 'LSTM', 'ALL'],
                  ),
]

nn_archi_types = [
    inquirer.List('NN_archi_type',
                  message="Which generative model architecture would you like to train ?",
                  choices=['Variational AutoEncoder', 'AutoEncoder'],
                  ),
]

# User can be choose between training and testing Auto Encoder models
nn_train_or_test_choices = [
    inquirer.List('nn_train_or_test',
                  message="Do you want to train or test Auto Encoders ?",
                  choices=['Train', 'Test'],
                  ),
]

def prompt_choices_to_user():
    window_size_lst = config.constants.get("WINDOW_SIZE_LST")
    nn_train_or_test = inquirer.prompt(nn_train_or_test_choices)["nn_train_or_test"]

    if nn_train_or_test == 'Train':
        nn_archi_type = inquirer.prompt(nn_archi_types)["NN_archi_type"]
        if nn_archi_type == 'AutoEncoder':
            model_dir = 'auto_encoder'
            # Compute the model choosen by the user
            nn_type = inquirer.prompt(nn_model_types)["NN_model_type"]
            if nn_type == 'CNN':
                print('Computing CNN model ...')
                nn_train('CNN', 'cnn', window_size_lst, model_dir)
            elif nn_type == 'MLP':
                print('Computing MLP model ...')
                nn_train('MLP', 'mlp', window_size_lst, model_dir)
            elif nn_type == 'RNN':
                print('Computing RNN model ...')
                nn_train('RNNAE', 'rnnae', window_size_lst, model_dir)
            elif nn_type == 'LSTM':
                print('Computing LSTM model ...')
                nn_train('LSTMAE', 'lstmae', window_size_lst, model_dir)
            elif nn_type == 'RAE-CNN':
                print('Computing Replacement Auto Encoder CNN model ...')
                nn_train('CNN_RAE', 'cnn_rae', window_size_lst, model_dir)
            # Compute all models
            elif nn_type == 'ALL':
                pass
        elif nn_archi_type == 'Variational AutoEncoder':
            model_dir = 'variational_auto_encoder'
            print('Computing MLP VAE model ...')
            nn_train('MLPVAE', 'mlpvae', window_size_lst, model_dir)
    elif nn_train_or_test == 'Test':
        test()


if __name__ == '__main__':
    prompt_choices_to_user()





